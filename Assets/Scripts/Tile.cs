﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Assertions;

public class Tile : MonoBehaviour
{
    public const int TILE_WIDTH = 32;
    public const int TILE_HEIGHT = 32;

    [SerializeField]
    private Sprite unableToUseSprite;
    [SerializeField]
    private Sprite[] smokeSprites;

    [SerializeField]
    private SpriteRenderer terrainRenderer;
    [SerializeField]
    private SpriteRenderer mountainRenderer;
    [SerializeField]
    private SpriteRenderer bigShadowRenderer;

    [SerializeField]
    private SpriteRenderer uiRenderer;
    [SerializeField]
    private SpriteRenderer fxRenderer;

    [SerializeField]
    private FrameAnimation frameAnimation;
    [SerializeField]
    private FrameAnimation fxAnimation;

    [SerializeField]
    private SpriteOutline spriteOutline;
    [SerializeField]
    private Color regularOutlineColor;
    [SerializeField]
    private Color errorOutlineColor;

    private Location location;
    public Location Location
    {
        get
        {
            return location;
        }
    }

    [SerializeField]
    private Building building;
    [SerializeField]
    private GameObject landingPrefab;

    public Building Building
    {
        get
        {
            return building;
        }
    }

    public bool HasBuilding
    {
        get
        {
            return building.HasBuilding;
        }
    }

    private ETerrainType terrainType;
    public ETerrainType TerrainType
    {
        get
        {
            return terrainType;
        }
    }
    private bool isInShadow;
    public bool IsInShadow
    {
        get
        {
            return isInShadow;
        }
    }

    public bool HasWater
    {
        get
        {
            return terrainType == ETerrainType.Water && !HasBuilding;
        }
    }

    private bool isSelected;

    private bool displayTerrainMode;

    public bool TryDryTerrain(Tile ignoreTile = null, bool forceDry = false)
    {
        //Debug.Log("TRY DRY " + (TerrainType == ETerrainType.Grass) + " = " + !HasWaterAround(ignoreTile) + " = " + HasFactoryAround());
        if (TerrainType == ETerrainType.Grass && (forceDry || !HasWaterAround(ignoreTile) || HasFactoryAround()))
            return true;
        return false;
    }

    private bool IsDemolishPossible
    {
        get
        {
            bool energyPlantDisableAllowed = HasBuilding && GameManager.Instance.PlayData.GetResourceValue(EResourceType.Energy) >= building.Data.GetBuildingBonus(EResourceType.Energy, building.Level);
            bool houseDemolishAllowed = HasBuilding && (GameManager.Instance.PlayData.houseCapacity - GameManager.Instance.PlayData.Population - building.Data.bonusCapacity * building.Data.GetEfficiency(building.Level)) >= 0;
            return DataManager.Instance.GetBuildingData(EBuildingType.PowerLine).IsBuildPossible(terrainType, isInShadow) && HasBuilding && Building.Type != EBuildingType.Base && Building.Type != EBuildingType.PowerLine
                && (Building.Type != EBuildingType.EnergyPlant || energyPlantDisableAllowed) && (Building.Type != EBuildingType.House || houseDemolishAllowed);
        }
    }
    
    private bool HasFactoryAround()
    {
        var neighbors = location.GetNeighbors(GameManager.Instance.CurrentLevel.Dimension);
        foreach (var neighbor in neighbors)
        {
            var tile = GameManager.Instance.CurrentLevel.GetTile(neighbor);
            if (tile != null && tile.HasBuilding && tile.building.Type == EBuildingType.Factory)
                return true;
        }
        return false;
    }

    private bool HasWaterAround(Tile ignoreTile = null)
    {
        var neighbors = location.GetNeighbors(GameManager.Instance.CurrentLevel.Dimension);
        foreach (var neighbor in neighbors)
        {
            var tile = GameManager.Instance.CurrentLevel.GetTile(neighbor);
            if (tile == null || (ignoreTile != null && ignoreTile.Location == tile.Location))
                continue;
            if (tile.HasWater)
                return true;
        }
        return false;
    }

    private Action<Tile, ETerrainType> onApplyTerrain;

    private List<Tile> linkedHoverTiles = new List<Tile>();

    private void DisplayDryWaterPreview(Tile ignoreTile, bool forceDry)
    {
        var neighbors = location.GetNeighbors(GameManager.Instance.CurrentLevel.Dimension);
        foreach (var neighbor in neighbors)
        {
            var tile = GameManager.Instance.CurrentLevel.GetTile(neighbor);
            if (tile != null && tile.TryDryTerrain(ignoreTile, forceDry))
            {
                if (tile.HasBuilding && tile.building.Type == EBuildingType.Farm)
                    tile.Building.Display(tile.Building.Data, 1, false, true);
                tile.DisplayTerrain(ETerrainType.Sand);
                linkedHoverTiles.Add(tile);
            }
        }
    }

    public void OnPointerEnter()
    {
        uiRenderer.enabled = false;
        var buildingData = GameManager.Instance.SelectedBuilding;
        var terraformData = GameManager.Instance.SelectedTerraform;

        if (buildingData != null)
        {
            if (buildingData.IsBuildPossible(terrainType, isInShadow) && (!HasBuilding || (Building.Type == EBuildingType.PowerLine && buildingData.type != EBuildingType.PowerLine)) && (IsConnectedToPower() || buildingData.type == EBuildingType.Base) && GameManager.Instance.PlayData.IsEnoughResources(buildingData))
            {
                Building.Display(buildingData, 1, true, true);
                ToggleSpriteOutline(true);
                if (buildingData.isLeveledUp)
                {
                    var synergyTiles = GameManager.Instance.CurrentLevel.GetNeighborSynergyTiles(location, buildingData);
                    if (synergyTiles.Count > 0)
                    {
                        linkedHoverTiles.AddRange(synergyTiles);
                        Building.DisplayBuildingBonus(buildingData.GetEfficiency(synergyTiles.Count + 1));
                        foreach (var tile in synergyTiles)
                        {
                            tile.TryDisplayBuildingBonus(1);
                            tile.ToggleSpriteOutline(true);
                        }
                    }
                }
                if (buildingData.dryWaterAround)
                    DisplayDryWaterPreview(terrainType == ETerrainType.Water ? this : null, buildingData.type == EBuildingType.Factory);
            }
            else
            {
                ToggleErrorTile(true);
            }
        }
        else if (GameManager.Instance.isDemolishing)
        {
            if (IsDemolishPossible)
            {
                Building.Display(DataManager.Instance.GetBuildingData(EBuildingType.PowerLine), 1, true, true);
                ToggleSpriteOutline(true);
                if (Building.Data.isLeveledUp)
                {
                    var synergyTiles = GameManager.Instance.CurrentLevel.GetNeighborSynergyTiles(location, Building.Data);
                    if (synergyTiles.Count > 0)
                    {
                        linkedHoverTiles.AddRange(synergyTiles);
                        foreach (var tile in synergyTiles)
                        {
                            tile.TryDisplayBuildingBonus(-1);
                            tile.ToggleSpriteOutline(true);
                        }
                    }
                }
            }
            else
                ToggleErrorTile(true);
        }
        else
        {
            uiRenderer.enabled = false;
            //DisplayBuilding(building);
            ToggleSpriteOutline(true);
            //TryDisplayBuildingBonus();
        }
    }

    public void OnPointerExit()
    {
        foreach (var tile in linkedHoverTiles)
            tile.OnPointerExit();
        linkedHoverTiles.Clear();
        if (!isSelected)
            ToggleSpriteOutline(false);
        else
            ToggleSpriteOutline(true, false, true);
        ToggleErrorTile(false);
        DisplayTerrain(terrainType);
        Building.HideBuildingBonus();
        if (!UIManager.Instance.IsBlocked)
            Building.Display();
    }

    public void OnSelect()
    {
        displayTerrainMode = isSelected ? !displayTerrainMode : false;
        ToggleSpriteOutline(true, false, true);
        isSelected = true;
        if (HasBuilding && !displayTerrainMode)
            UIManager.Instance.infoPanel.Init(Building);
        else
            UIManager.Instance.infoPanel.Init(this);
    }

    public void OnDeselect()
    {
        isSelected = false;
        ToggleSpriteOutline(false);
        UIManager.Instance.infoPanel.Hide();

    }

    public void TryDisplayBuildingBonus(int extraLevel = 0)
    {
        if (HasBuilding && Building.IsLeveledUp)
            Building.DisplayBuildingBonus(Building.GetEfficiency(Building.Level + extraLevel));
    }

    public void SetInShadow(bool active)
    {
        isInShadow = active;
        Building.OnTerrainChange(terrainType, active);
    }

    public bool IsConnectedToPower()
    {
        var neighbors = location.GetCrossNeighbors(GameManager.Instance.CurrentLevel.Dimension);
        foreach (var neighbor in neighbors)
            if (GameManager.Instance.CurrentLevel.GetTile(neighbor).HasBuilding)
                return true;
        return false;
    }

    public void TryDemolish()
    {
        if (IsDemolishPossible)
            Building.Deconstruct();
    }

    public void TryBuildABuilding(BuildingData buildingData)
    {    
        Assert.IsTrue(buildingData != null);
        if (buildingData.IsBuildPossible(TerrainType, isInShadow) && (!HasBuilding || (Building.Type == EBuildingType.PowerLine && buildingData.type != EBuildingType.PowerLine)) && (IsConnectedToPower() || buildingData.type == EBuildingType.Base) && GameManager.Instance.PlayData.IsEnoughResources(buildingData))
        {
            if (buildingData.isLeveledUp)
            {
                Building.Build(buildingData, location, OnBuildingWasChanged);
                if (Building.UpdateLevel())
                    Building.UpdateNeighbors();
            }
            else
                Building.Build(buildingData, location, OnBuildingWasChanged);
            var neighbors = GameManager.Instance.CurrentLevel.GetCrossTiles(location);
            foreach (var neighbor in neighbors)
                neighbor.Building.Display(neighbor.Building.Data, neighbor.Building.Level, neighbor.Building.IsEnabled);
            spriteOutline.enabled = false;
            if (buildingData.type == EBuildingType.Base)
            {
                building.isLanding = true;
                building.Hide();
                UIManager.Instance.buildingsPanel.Reset(false);
                GameManager.Instance.tileInputHandler.DeselectTile();
                GameManager.Instance.landingAnimation.PlayAnimation(location, OnLandingFinished);
            }
            else
            {
                Building.Display(buildingData, Building.Level, true);
                PlayTerraformAnimation();
                UIManager.Instance.infoPanel.Init(buildingData);
                UIManager.Instance.resourcePanelUI.UpdateData(GameManager.Instance.PlayData);
            }
        }
    }

    private void OnLandingFinished()
    {
        building.isLanding = false;
        Building.Display(DataManager.Instance.GetBuildingData(EBuildingType.Base), Building.Level, true);
        PlayTerraformAnimation();
        UIManager.Instance.resourcePanelUI.UpdateData(GameManager.Instance.PlayData);
    }

    private void OnBuildingWasChanged()
    {
        PlayTerraformAnimation();
    }

    public void DisplayTerraformIcon(ETerraformType terraformType, bool isError = false)
    {
        if (terraformType == ETerraformType.None)
            uiRenderer.sprite = null;
        else
            uiRenderer.sprite = isError ? DataManager.Instance.terraformCrossIcons.GetSprite(terraformType.ToString()) : DataManager.Instance.terraformIcons.GetSprite(terraformType.ToString());
        uiRenderer.enabled = true;
    }


    public void ToggleSpriteOutline(bool active, bool errorColor = false, bool selectColor = false)
    {
        //Debug.Log(" ToggleSpriteOutline " + active + " = " + errorColor);
        spriteOutline.color = errorColor ? errorOutlineColor : (selectColor ? Color.green : regularOutlineColor);
        //spriteOutline.outlineSize = selectColor ? 2 : 1;
        spriteOutline.enabled = active;
    }

    public void ToggleErrorTile(bool active)
    {
        uiRenderer.sprite = unableToUseSprite;
        uiRenderer.enabled = active;
    }

    public void Init(ETerrainType terrainType, Location tileLocation, Action<Tile, ETerrainType> onApplyTerrain)
    {
        this.onApplyTerrain = onApplyTerrain;
        location = tileLocation;
        transform.position = new Vector3(tileLocation.x * TILE_WIDTH, tileLocation.y * TILE_HEIGHT);
        SetTerrain(terrainType, false);
        UpdateOrder();
    }

    public bool TryTerraformTerrain(ETerraformType terraformType, out ETerrainType outputTerrain)
    {
        outputTerrain = terrainType;
        if (HasBuilding && terraformType != ETerraformType.None)
            return false;
        switch (terraformType)
        {
            case ETerraformType.Destroy:
                {
                    if (terrainType == ETerrainType.Rock || terrainType == ETerrainType.Metal)
                    {
                        outputTerrain = ETerrainType.Sand;
                        return true;
                    }
                    else
                    {
                        outputTerrain = terrainType;
                        return true;
                    }
                }
            case ETerraformType.Metal:
                {
                    if (terrainType == ETerrainType.Sand || terrainType == ETerrainType.Metal)
                    {
                        outputTerrain = ETerrainType.Metal;
                        return true;
                    }
                    break;
                }
            case ETerraformType.Mountain:
                {
                    if (terrainType == ETerrainType.Wasteland || terrainType == ETerrainType.Sand || terrainType == ETerrainType.Rock || terrainType == ETerrainType.Grass || terrainType == ETerrainType.Metal)
                    {
                        outputTerrain = ETerrainType.Rock;
                        return true;
                    }
                    break;
                }
            case ETerraformType.Wasteland:
                {
                    if (terrainType != ETerrainType.Water && terrainType != ETerrainType.Rock)
                    {
                        outputTerrain = ETerrainType.Wasteland;
                        return true;
                    }
                    if (terrainType == ETerrainType.Rock)
                    {
                        outputTerrain = ETerrainType.Rock;
                        return true;
                    }
                    break;
                }
            case ETerraformType.Water:
                {
                    if (terrainType == ETerrainType.Sand || terrainType == ETerrainType.Grass || terrainType == ETerrainType.Metal || terrainType == ETerrainType.Water)
                    {
                        outputTerrain = ETerrainType.Water;
                        return true;
                    }
                    if (terrainType == ETerrainType.Wasteland)
                    {
                        outputTerrain = ETerrainType.Sand;
                        return true;
                    }
                    break;
                }
            case ETerraformType.None:
                {
                    outputTerrain = terrainType;
                    return true;
                }
        }
        return false;
    }

    public void PlayTerraformAnimation()
    {
        fxAnimation.PlaySingle(fxRenderer, smokeSprites, 8f);
    }

    public void SetTerrain(ETerrainType newTerrainType, bool affectNeighbors = true, bool playEffectAnimation = false)
    {
        //Debug.Log("SET TERRAIN " + newTerrainType + " = " + location);
        if (playEffectAnimation)
            PlayTerraformAnimation();
        terrainType = newTerrainType;    
        Building.OnTerrainChange(terrainType, isInShadow);
        if (newTerrainType == ETerrainType.Sand)
        {
            var neighbors = location.GetNeighbors(GameManager.Instance.CurrentLevel.Dimension);
            foreach (var neighbor in neighbors)
                if (GameManager.Instance.CurrentLevel.GetTile(neighbor).HasWater && !HasFactoryAround())
                {
                    //Debug.Log("SAND transformed into GRASS");
                    terrainType = ETerrainType.Grass;
                    break;
                }
        }
        if (terrainType != ETerrainType.Rock)
        {
            var targetTile = GameManager.Instance.CurrentLevel.GetTile(new Location(location.x + 1, location.y));
            if (targetTile != null)
            {
                targetTile.SetInShadow(false);
            }
        }
        DisplayTerrain(terrainType);
        if (affectNeighbors)
            onApplyTerrain(this, terrainType);
        var tile = GameManager.Instance.CurrentLevel.GetTile(new Location(location.x - 1, location.y));
        if (tile != null)
            tile.UpdateMountainShadowLayer();
        UpdateMountainShadowLayer();
    }

    public void DisplayTerrain(ETerrainType terrainType, bool isPreview = false)
    {
        terrainRenderer.color = new Color(1f, 1f, 1f, isPreview ? 1f : 1f);
        mountainRenderer.color = new Color(1f, 1f, 1f, isPreview ? 1f : 1f);
        bigShadowRenderer.color = new Color(1f, 1f, 1f, isPreview ? 1f : 1f);
        var data = DataManager.Instance.GetTerrainData(terrainType);
        if (terrainType == ETerrainType.Rock)
        {
            frameAnimation.PlayLoop(terrainRenderer, data.terrainSprites);
            mountainRenderer.sprite = data.topSprites[0];
            bigShadowRenderer.sprite = data.shadowSprites[0];
            bigShadowRenderer.enabled = (location.x + 1) < GameManager.Instance.CurrentLevel.Dimension.x;
        }
        else
        {
            frameAnimation.PlayLoop(terrainRenderer, data.terrainSprites, data.animationSpeed);
            mountainRenderer.sprite = null;
            bigShadowRenderer.sprite = null;
        }
    }

    public void UpdateMountainShadowLayer()
    {
        var targetTile = GameManager.Instance.CurrentLevel.GetTile(new Location(location.x + 1, location.y));
        if (targetTile != null)
        {
            bigShadowRenderer.sortingLayerName = targetTile.TerrainType == ETerrainType.Rock ? "Shadow" : "Building";
        }
    }

    private void UpdateOrder()
    {
        var width = GameManager.Instance.CurrentLevel.Dimension.x;
        var sortingOrder = (-location.y * width - location.x) * 3 + 300;
        terrainRenderer.sortingOrder = sortingOrder;
        bigShadowRenderer.sortingOrder = sortingOrder;
        bigShadowRenderer.sortingOrder = sortingOrder;
        mountainRenderer.sortingOrder = sortingOrder;
        fxRenderer.sortingOrder = sortingOrder + 2;
        Building.UpdateOrder(sortingOrder);
    }
}
