﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class StoryEvent : GameEvent
{
    public int imageIndex;
    public string textCaption;
}
