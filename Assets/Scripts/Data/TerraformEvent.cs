﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TerraformEvent : GameEvent
{
    public List<ETerraformClass> terraformClasses;
    public int numberOfPatterns;

    public ETerraformClass RandomClass
    {
        get
        {
            return terraformClasses[Random.Range(0, terraformClasses.Count)];
        }
    }
}
