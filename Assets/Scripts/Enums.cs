﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EDirection
{
    Top,
    Right,
    Bottom,
    Left
}

public enum ETerrainType
{
    Sand,
    Water,
    Grass,
    Rock,
    Wasteland,
    Metal    
}

public enum EBuildingType
{
    Factory,
    Farm,
    EnergyPlant,
    House,
    WaterPump,
    Base,
    PowerLine
}

public enum EResourceType
{
    Metal,
    Food,
    Energy,
    Water,
    Workers
}

public enum ETerraformType
{
    None,
    Water,
    Metal,
    Wasteland,
    Destroy,
    Mountain
}

public enum ETerraformClass
{
    Water,
    WaterHard,
    Metal,
    MetalHard,
    Transform,
    TransformHard,
    Junk
}

public enum EDifficulty
{
    Casual,
    Easy,
    Medium,
    Hard,
    Sandbox
}

