﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TerraformPatternRow
{
    public List<ETerraformType> elements = new List<ETerraformType>();
}
