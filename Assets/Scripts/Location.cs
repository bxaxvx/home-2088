﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public struct Location
{
    public int x;
    public int y;

    public Location(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

    public override string ToString()
    {
        return "(" + x + ", " + y + ")";
    }

    public static Location Zero
    {
        get { return new Location(0, 0); }
    }

    public int SqrMagnitude
    {
        get
        {
            return x * x + y * y;
        }
    }

    public override bool Equals(object obj)
    {
        Location targetLocation = (Location)obj;
        return targetLocation.x == x && targetLocation.y == y;
    }

    public override int GetHashCode()
    {
        return new Vector2(x,y).GetHashCode();
    }

    public static bool operator ==(Location c1, Location c2)
    {
        return c1.Equals(c2);
    }

    public static Location operator +(Location c1, Location c2)
    {
        return new Location(c1.x +c2.x, c1.y+c2.y);
    }

    public static Location operator -(Location c1, Location c2)
    {
        return new Location(c1.x - c2.x, c1.y - c2.y);
    }

    public static bool operator !=(Location c1, Location c2)
    {
        return !c1.Equals(c2);
    }

    public Vector3 ToWorldCoordinates()
    {
        return new Vector3(x * Tile.TILE_WIDTH, y * Tile.TILE_HEIGHT, 10f);
    }

    public Location GetNeighbor(EDirection direction)
    {
        switch (direction)
        {
            case EDirection.Bottom:
                {
                    return new Location(x, y - 1);
                }
            case EDirection.Right:
                {
                    return new Location(x + 1, y);
                }
            case EDirection.Top:
                {
                    return new Location(x, y + 1);
                }
            case EDirection.Left:
                {
                    return new Location(x - 1, y);
                }
        }
        Assert.IsTrue(true);
        return this;
    }

    public List<Location> GetNeighbors(Location maxLocation)
    {
        List<Location> neighbors = new List<Location>();
        for (int i = -1; i <= 1; i++)
            for (int j = -1; j <= 1; j++)
                if (new Location(x + i, y + j).IsInSquare(Location.Zero, maxLocation))
                    neighbors.Add(new Location(x + i, y + j));
        neighbors.Remove(this);
        return neighbors;
    }

    public List<Location> GetCrossNeighbors(Location maxLocation)
    {
        List<Location> neighbors = new List<Location>();
        if (new Location(x, y + 1).IsInSquare(Location.Zero, maxLocation))
            neighbors.Add(new Location(x, y + 1));
        if (new Location(x + 1, y).IsInSquare(Location.Zero, maxLocation))
            neighbors.Add(new Location(x + 1, y));
        if (new Location(x, y - 1).IsInSquare(Location.Zero, maxLocation))
            neighbors.Add(new Location(x, y - 1));
        if (new Location(x - 1, y).IsInSquare(Location.Zero, maxLocation))
            neighbors.Add(new Location(x - 1, y));
        return neighbors;
    }

    public bool IsInSquare(Location minLocation, Location maxLocation)
    {
        //Debug.Log(this.ToString() + " = " + minLocation.ToString() + " = " + maxLocation.ToString() + " = " + (x >= minLocation.x && x <= maxLocation.x && y >= minLocation.y && y <= maxLocation.y));
        return (x >= minLocation.x && x < maxLocation.x && y >= minLocation.y && y < maxLocation.y);
    }
}
