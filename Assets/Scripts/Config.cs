﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Config
{
    public static int startFood = 100;
    public static int startWater = 100;
    public static int startMetal = 160;

    public static int generatorWaterNumber = 10;
    public static int generatorMetalNumber = 5;
    public static float metalSquareRadius = 36;
    public static float waterSquareRadius = 16;
    public static float waterToMetalMinDistance = 4;

    public static int generatorMapWidth = 28;
    public static int generatorMapHeight = 16;
}
