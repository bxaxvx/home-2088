﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class HelperTools
{
    ///<summary>
    ///<para>Instantiate additional GameObjects from prefab if not enough, or disable redundant GameObjects.</para>
    ///</summary>
    public static void NormalizePool<T>(ref List<T> pool, GameObject prefab, Transform parent, int newLength) where T : MonoBehaviour
    {
        if (pool == null)
            pool = new List<T>();
        foreach (var item in pool)
            item.gameObject.SetActive(true);

        int poolLength = pool.Count;
        for (int i = 0; i < newLength - poolLength; i++)
        {
            T newObject = (Object.Instantiate(prefab) as GameObject).GetComponent<T>();
            newObject.transform.SetParent(parent, false);
            pool.Add(newObject);
        }

        poolLength = pool.Count;
        for (int i = 0; i < poolLength - newLength; i++)
        {
            pool[i + newLength].gameObject.SetActive(false);
        }
    }

    ///<summary>
    ///<para>Returns sprite from array by its name.</para>
    ///</summary>
    public static Sprite GetSprite(this Sprite[] sprites, string name)
    {
        foreach (var sprite in sprites)
        {
            if (sprite == null)
                continue;
            if (sprite.name.Replace(" ", "").Replace("_", "").ToLower().Contains(name.Replace(" ", "").Replace("_", "").ToLower()))
                return sprite;
        }
        // Upgrading this to an error as warnings are ignored and since this returns an incorrect sprite it is easy to not notice that it hasn't been set
        Debug.LogError("Can't find '" + name + "' sprite");
        return sprites.Length > 0 ? sprites[0] : null;
    }

    ///<summary>
    ///<para>Returns object from array by its name.</para>
    ///</summary>
    public static T GetObject<T>(this T[] objects, string name) where T : Object
    {
        foreach (var obj in objects)
        {
            if (obj == null)
                continue;
            if (obj.name.Replace(" ", "").Replace("_", "").ToLower().Contains(name.Replace(" ", "").Replace("_", "").ToLower()))
                return obj;
        }
        // Upgrading this to an error as warnings are ignored and since this returns an incorrect sprite it is easy to not notice that it hasn't been set
        Debug.LogError("Can't find '" + name + "' object");
        return objects.Length > 0 ? objects[0] : null;
    }


    public static int FindClosestNumberIndexInList(this List<float> list, float toNumber)
    {
        float bestValue = float.MaxValue;
        int bestValueIndex = 0;
        int index = 0;
        foreach (var num in list)
        {
            float newValue = Mathf.Abs(num - toNumber);
            if (newValue < bestValue)
            {
                bestValue = newValue;
                bestValueIndex = index;
            }
            index++;
        }
        return bestValueIndex;
    }

    public static float FindClosestNumberInList(this List<float> list, float toNumber)
    {
        return list[FindClosestNumberIndexInList(list, toNumber)];
    }

    public static ETerraformType[,] RotateMatrixCounterClockwise(this ETerraformType[,] oldMatrix)
    {
        ETerraformType[,] newMatrix = new ETerraformType[oldMatrix.GetLength(1), oldMatrix.GetLength(0)];
        int newColumn, newRow = 0;
        for (int oldColumn = oldMatrix.GetLength(1) - 1; oldColumn >= 0; oldColumn--)
        {
            newColumn = 0;
            for (int oldRow = 0; oldRow < oldMatrix.GetLength(0); oldRow++)
            {
                newMatrix[newRow, newColumn] = oldMatrix[oldRow, oldColumn];
                newColumn++;
            }
            newRow++;
        }
        return newMatrix;
    }

    public static string FormatValue<T>(this T value, T normalValue, string suffix ="") where T : System.IComparable
    {
        return string.Format("<color={0}><b>{1}</b></color>", value.CompareTo(normalValue) == 0 ? "white" : (value.CompareTo(normalValue) > 0 ? "green" : "red"), value + suffix);
    }
}
