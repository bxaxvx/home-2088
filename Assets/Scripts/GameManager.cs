﻿using EZCameraShake;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    private static GameManager instance;

    public static GameManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<GameManager>();
            }

            return instance;
        }
    }

    public TileInputHandler tileInputHandler;
    [SerializeField]
    private Transform levelTransform;
    [SerializeField]
    private GameObject tilePrefab;
    [SerializeField]
    private GameObject linePrefab;
    [SerializeField]
    private GameObject borderTilePrefab;

    public LandingAnimation landingAnimation;

    private Level currentLevel;
    public Level CurrentLevel
    {
        get
        {
            return currentLevel;
        }
    }

    private PlayData playData;
    public PlayData PlayData
    {
        get
        {
            return playData;
        }
    }

    [HideInInspector]
    public bool isDemolishing;
    private BuildingData selectedBuilding;
    public bool IsBuildingMode
    {
        get
        {
            return selectedBuilding != null || isDemolishing;
        }
    }
    public BuildingData SelectedBuilding
    {
        get { return selectedBuilding; }
    }

    private TerraformData selectedTerraform;
    public TerraformData SelectedTerraform
    {
        get { return selectedTerraform; }
    }

    public ScenarioData scenarioData;

    public void StartGame(EDifficulty difficulty)
    {
        playData = new PlayData(difficulty);
        currentLevel = new Level(tilePrefab, linePrefab, borderTilePrefab, levelTransform);
        currentLevel.LoadRandomLevel();
        UIManager.Instance.resourcePanelUI.UpdateData(playData);
        UIManager.Instance.eventAnnouncer.OnTurnMade(1, scenarioData);
        UIManager.Instance.modalDialog.Show("Welcome", "Greetings!\n\nYou were sent from Earth to prepare the planet for the future settlers. Start your colony by establishing a Base. After that be sure to build enough Houses for the newcomers, and prepare enough Food and Water supplies.\n\nGood luck!");
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.F4))
        {
            UIManager.Instance.gameObject.SetActive(!UIManager.Instance.gameObject.activeSelf);
        }
    }

    public void ProceedNextTurn()
    {
        if (SelectedTerraform != null)
        {
            UIManager.Instance.patternsToggleGroup.SetAllTogglesOff();
            ResetTilesUI();
        }
        else if (IsBuildingMode)
        {
            UIManager.Instance.buildingsPanel.Reset();
            tileInputHandler.ResetCurrentTile();
        }
        UIManager.Instance.CancelPattern();
        playData.CalculateTurn();
        foreach (var storyEvent in scenarioData.storyEvents)
        {
            if (storyEvent.turnNumber == playData.turnNumber)
            {
                UIManager.Instance.storyUI.Display(storyEvent.imageIndex, storyEvent.textCaption);
                break;
            }
        }

        bool regularDay = true;
        foreach (var peopleEvent in scenarioData.peopleEvents)
        {
            if (peopleEvent.turnNumber == (playData.turnNumber % (scenarioData.peopleEvents[scenarioData.peopleEvents.Count-1].turnNumber + 1)))
            {
                UIManager.Instance.storyUI.Display(8, "New colonists arrived from Earth.");
                if (playData.FreeHousePlaces >= peopleEvent.numberOfArrivedPeople)
                    UIManager.Instance.modalDialog.Show("Day " + playData.turnNumber, "Today " + peopleEvent.numberOfArrivedPeople + " new colonists arrived. \n\nIt's great that we have free houses prepared for them.",
                        string.Format("Take {0} colonists", peopleEvent.numberOfArrivedPeople), () => playData.AddPopulation(peopleEvent.numberOfArrivedPeople),
                        string.Format("Take {0} colonists", peopleEvent.numberOfArrivedPeople / 2), () =>  playData.AddPopulation(peopleEvent.numberOfArrivedPeople / 2),                                               
                        string.Format("We're not ready"));
                else if (playData.FreeHousePlaces >= (peopleEvent.numberOfArrivedPeople / 2))
                    UIManager.Instance.modalDialog.Show("Day " + playData.turnNumber, "Today " + peopleEvent.numberOfArrivedPeople + " new colonists arrived. \n\nUnfortunately, we don't have space for everyone.",                                                
                        string.Format("Take {0} colonists", peopleEvent.numberOfArrivedPeople / 2), () => playData.AddPopulation(peopleEvent.numberOfArrivedPeople / 2),                                              
                        string.Format("We're not ready"));
                else
                    UIManager.Instance.modalDialog.Show("Day " + playData.turnNumber, "Today " + peopleEvent.numberOfArrivedPeople + " new colonists arrived. \n\nUnfortunately, we don't have any free houses left.",
                        "We're not ready");
                regularDay = false;
                break;
            }
        }
        foreach (var terraformEvent in scenarioData.terraformEvents)
        {
            if (terraformEvent.turnNumber == (playData.turnNumber % (scenarioData.terraformEvents[scenarioData.terraformEvents.Count - 1].turnNumber + 1)))
            {
                UIManager.Instance.DisplayTerraformOptions(terraformEvent.numberOfPatterns, terraformEvent.terraformClasses);
                if (regularDay)
                    UIManager.Instance.modalDialog.Show("Day " + playData.turnNumber, "Today we received new terraforming supplies from Earth.\n\nUse wisely!");
                else
                    UIManager.Instance.modalDialog.hideCallback = () => UIManager.Instance.modalDialog.Show("Day " + playData.turnNumber, "Today we received new terraforming supplies from Earth.\n\nUse wisely!");
                regularDay = false;
                break;
            }
        }
        if (regularDay)
            UIManager.Instance.modalDialog.Show("Day " + playData.turnNumber, "Nothing special happened today.");

        UIManager.Instance.resourcePanelUI.UpdateData(playData);
        UIManager.Instance.eventAnnouncer.OnTurnMade(playData.turnNumber, scenarioData);
        if (playData.turnNumber > scenarioData.TurnLimit(playData.difficulty))
            GameOver(0);
        if (playData.GetResourceValue(EResourceType.Food) < 0)
            GameOver(1);
        if (playData.GetResourceValue(EResourceType.Water) < 0)
            GameOver(2);
        if (playData.Population >= scenarioData.PopulationGoal(playData.difficulty))
            Victory();
    }

    private void GameOver(int loseType)
    {
        string description = "";
        if (loseType == 0)
            description = "Sadly, you failed to accomplish the goal that was set by the Board.\n\nYou lost.";
        else if (loseType == 1)
            description = "Sadly, our colony is out of food.\n\nYou lost.";
        else
            description = "Sadly, our colony is out of water.\n\nYou lost.";

        UIManager.Instance.modalDialog.Show("Game Over", description, "Try again", RestartGame, "Quit", Application.Quit);
    }

    public void RestartGame()
    {
        SceneManager.LoadScene("Main", LoadSceneMode.Single);
    }

    public void Victory()
    {
        UIManager.Instance.storyUI.Display(6, "Victory!");
        UIManager.Instance.storyUI.Display(7, "This was unappealing place, but now we are finally feeling at home!");
        string description = "Well done!\n\n You built and sustained a self sufficient colony.";
        UIManager.Instance.modalDialog.Show("Victory", description, "Try again", RestartGame, "Quit", Application.Quit);
    }

    public void OnBuildSelected(EBuildingType buildingType)
    {
        tileInputHandler.DeselectTile();
        selectedBuilding = DataManager.Instance.GetBuildingData(buildingType);
    }

    public void OnBuildDeselected(EBuildingType buildingType)
    {
        selectedBuilding = null;
    }

    public void OnTerraformSelected(TerraformData data)
    {
        tileInputHandler.DeselectTile();
        selectedTerraform = data;
    }

    public void OnTerraformDeselected(TerraformData data)
    {
        selectedTerraform = null;
    }

    public void ResetTilesUI()
    {
        for (int i = 0; i < currentLevel.Dimension.x; i++)
            for (int j = 0; j < currentLevel.Dimension.y; j++)
            {
                var tile = currentLevel.GetTile(new Location(i, j));
                Assert.IsNotNull(tile);
                tile.ToggleErrorTile(false);
                tile.ToggleSpriteOutline(false);
                tile.DisplayTerrain(tile.TerrainType);
            }
    }

    public void TryTerraform(Location startTile)
    {
        bool terraformPossible;
        var terraformedTerrain = DisplayTerraform(startTile, out terraformPossible);
        if (terraformPossible)
        {
            SoftShake();
            int height = selectedTerraform.pattern.Count;
            int width = selectedTerraform.pattern[0].elements.Count;
            for (int j = 0; j < height; j++)
            {
                for (int i = 0; i < width; i++)
                {
                    var tile = currentLevel.GetTile(new Location(startTile.x + i, startTile.y + j));
                    Assert.IsNotNull(tile);
                    if (terraformedTerrain[j, i] != tile.TerrainType)
                        tile.PlayTerraformAnimation();
                    if (tile.TerrainType != terraformedTerrain[j, i])
                        tile.SetTerrain(terraformedTerrain[j, i]);
                }
            }
            UIManager.Instance.CancelPattern();
        }
    }

    public void SoftShake()
    {
        CameraShakeInstance shakeProperty = new CameraShakeInstance(7, 3, 0.3f, 0.7f);
        shakeProperty.PositionInfluence = new Vector3(1, 0.8f);
        shakeProperty.RotationInfluence = new Vector3(0, 0, 0.1f);
        CameraShaker.Instance.Shake(shakeProperty);
    }

    public void HardShake()
    {
        CameraShakeInstance shakeProperty = new CameraShakeInstance(12, 5, 0.5f, 1f);
        shakeProperty.PositionInfluence = new Vector3(1, 0.8f);
        shakeProperty.RotationInfluence = new Vector3(0, 0, 0.1f);
        CameraShaker.Instance.Shake(shakeProperty);
    }


    public ETerrainType[,] DisplayTerraform(Location startTile, out bool isTerraformPossible)
    {
        Assert.IsNotNull(selectedTerraform);
        int height = selectedTerraform.pattern.Count;
        int width = selectedTerraform.pattern[0].elements.Count;
        bool isOutOfBound = false;

        for (int j = 0; j < height; j++)
        {
            if (isOutOfBound)
                break;
            for (int i = 0; i < width; i++)
            {
                if (currentLevel.GetTile(new Location(startTile.x + i, startTile.y + j)) == null)
                {
                    isOutOfBound = true;
                    break;
                }
            }
        }
        if (isOutOfBound)
        {
            isTerraformPossible = DisplayTerraform(startTile, true, false, null, null);
            return null;
        }

        bool[,] terraformPossibleMask = new bool[width, height];
        ETerrainType[, ] outputTerrainMask = new ETerrainType[width, height];
        bool foundError = false;
        for (int j = 0; j < height; j++)
        {
            for (int i = 0; i < width; i++)
            {
                var tile = currentLevel.GetTile(new Location(startTile.x + i, startTile.y + j));
                Assert.IsNotNull(tile);
                terraformPossibleMask[j,i] = tile.TryTerraformTerrain(selectedTerraform.GetTerraformType(height - 1 - j, i), out outputTerrainMask[j, i]);
                if (!terraformPossibleMask[j, i])
                    foundError = true;
            }
        }
        if (foundError)
        {
            isTerraformPossible = DisplayTerraform(startTile, false, true, terraformPossibleMask, outputTerrainMask);
            return null;
        }

        isTerraformPossible = DisplayTerraform(startTile, false, false, terraformPossibleMask, outputTerrainMask);
        return outputTerrainMask;
    }

    private bool DisplayTerraform(Location startTile, bool isOutOfBound, bool isTileError, bool[,] terraformPossibleMask, ETerrainType[,] outputTerrainMask)
    {
        //Debug.Log(isOutOfBound + " = " + isTileError);
        int height = selectedTerraform.pattern.Count;
        int width = selectedTerraform.pattern[0].elements.Count;
        for (int j = 0; j < height; j++)
        {
            for (int i = 0; i < width; i++)
            {
                var tile = currentLevel.GetTile(new Location(startTile.x + i, startTile.y + j));
                if (tile == null)
                    continue;
                tile.ToggleSpriteOutline(true, isOutOfBound || isTileError);
                tile.ToggleErrorTile(isOutOfBound);
                if (terraformPossibleMask != null)
                    tile.DisplayTerraformIcon(selectedTerraform.GetTerraformType(height - 1 - j, i), !terraformPossibleMask[j, i]);
                if (!isOutOfBound && !isTileError)
                    tile.DisplayTerrain(outputTerrainMask[j,i], true);
                else
                    tile.DisplayTerrain(tile.TerrainType);
            }
        }
        return !isOutOfBound && !isTileError;

    }
}
