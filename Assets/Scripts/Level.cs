﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level
{
    private Level() { }

    public Level(GameObject tilePrefab, GameObject linePrefab, GameObject borderTilePrefab, Transform levelHolder)
    {
        this.linePrefab = linePrefab;
        this.tilePrefab = tilePrefab;
        this.levelHolder = levelHolder;
        this.borderTilePrefab = borderTilePrefab;
    }

    private GameObject tilePrefab;
    private GameObject linePrefab;
    private GameObject borderTilePrefab;

    private Transform levelHolder;
    private int width;
    private int height;

    public Location Dimension
    {
        get
        {
            return new Location(width, height);
        }
    }

    private Tile[,] tiles;

    public Tile GetTile(Location location)
    {
        if (location.IsInSquare(Location.Zero, new Location(width, height)))
            return tiles[location.x, location.y];
        else
            return null;
    }

    private void SpawnLevel(ETerrainType[,] terrains, int width, int height)
    {
        tiles = new Tile[width, height];
        this.width = width;
        this.height = height;
        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                Tile newTile = Object.Instantiate(tilePrefab, levelHolder).GetComponent<Tile>();
                tiles[i, j] = newTile;
            }
        }
        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
                tiles[i, j].Init(terrains[i, j], new Location(i, j), OnApplyTerrain);
        }

        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                OnApplyTerrain(tiles[i, j], tiles[i,j].TerrainType);
            }
        }
        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                tiles[i, j].UpdateMountainShadowLayer();
            }
        }
        SpawnBorderTiles();
        SpawnLines(width, height);
    }

    private void SpawnBorderTiles()
    {
        for (int i = 0; i < width; i++)
        {
            var newTile = Object.Instantiate(borderTilePrefab, levelHolder).GetComponent<Transform>();
            newTile.position = new Vector3(Tile.TILE_WIDTH * i, -Tile.TILE_HEIGHT);
        }
    }

    public List<Tile> GetCrossTiles(Location location)
    {
        var neighbors = location.GetCrossNeighbors(new Location(width, height));
        List<Tile> tiles = new List<Tile>();
        foreach (var neighborLocation in neighbors)
        {
            var tile = GetTile(neighborLocation);
            if (tile != null)
                tiles.Add(tile);
        }
        return tiles;
    }
    public List<Tile> GetNeighborSynergyTiles(Location location, BuildingData buildingData)
    {
        var neighbors = location.GetCrossNeighbors(new Location(width, height));
        List<Tile> tiles = new List<Tile>();
        foreach (var neighborLocation in neighbors)
        {
            var tile = GetTile(neighborLocation);
            if (tile != null && tile.HasBuilding && tile.Building.Type == buildingData.type && tile.Building.IsEnabled)
                tiles.Add(tile);
        }
        return tiles;
    }

    private List<UILine> horizontalLines = new List<UILine>();


    private List<UILine> verticalLines = new List<UILine>();

    private void SpawnLines(int width, int height)
    {
        HelperTools.NormalizePool(ref horizontalLines, linePrefab, levelHolder, height - 1);
        HelperTools.NormalizePool(ref verticalLines, linePrefab, levelHolder, width - 1);

        for (int i = 1; i < height; i++)
            horizontalLines[i-1].DrawHorizontal(Tile.TILE_HEIGHT * i, width * Tile.TILE_WIDTH);
        for (int i = 1; i < width; i++)
            verticalLines[i-1].DrawVertical(Tile.TILE_WIDTH * i, height * Tile.TILE_HEIGHT);
    }

    private void OnApplyTerrain(Tile updatedTile, ETerrainType newTerrainType)
    {
        //Debug.Log("ON APPLY TERRAIN " + updatedTile.Location + " = " + newTerrainType);
        switch (newTerrainType)
        {
            case ETerrainType.Water:
                {
                    var neighbors = updatedTile.Location.GetNeighbors(new Location(width, height));
                    foreach (var neighbor in neighbors)
                        if (GetTile(neighbor).TerrainType == ETerrainType.Sand)
                        {
                            var neighborsLocs = neighbor.GetNeighbors(Dimension);
                            bool hasFactory = false;
                            foreach (var loc in neighborsLocs)
                            {
                                if (GetTile(loc).HasBuilding && GetTile(loc).Building.Type == EBuildingType.Factory)
                                {
                                    hasFactory = true;
                                    break;
                                }
                            }
                            if (!hasFactory)
                                GetTile(neighbor).SetTerrain(ETerrainType.Grass, true, true);
                        }
                    break;
                }
            case ETerrainType.Rock:
                {
                    var tile = GetTile(updatedTile.Location.GetNeighbor(EDirection.Right));
                    if (tile != null)
                        tile.SetInShadow(true);
                    break;
                }
            //case ETerrainType.Metal:
            //    {
            //        var neighbors = updatedTile.Location.GetNeighbors(new Location(width, height));
            //        foreach (var neighbor in neighbors)
            //            if (GetTile(neighbor).TryTerraformTerrain(ETerrainType.Sand))
            //                GetTile(neighbor).SetTerrain(ETerrainType.Sand); break;
            //    }
        }
    }

    public void LoadRandomLevel()
    {
        SpawnLevel(GenerateMap(Config.generatorMapWidth, Config.generatorMapHeight), Config.generatorMapWidth, Config.generatorMapHeight);
    }

    public ETerrainType[,] GenerateMap(int width, int height)
    {
        ETerrainType[,] terrains = new ETerrainType[width, height];
        List<Location> locations = new List<Location>();
        for (int i = 0; i < terrains.GetLength(0); i++)
        {
            for (int j = 0; j < terrains.GetLength(1); j++)
            {
                locations.Add(new Location(i, j));
            }
        }
        List<Location> waterLocations = new List<Location>(locations);
        List<Location> metalLocations = new List<Location>(locations);
        
        for (int index = 0; index < Config.generatorMetalNumber; index++)
        {
            if (metalLocations.Count > 0)
            {
                var spawnLocation = metalLocations[Random.Range(0, metalLocations.Count)];
                for (int i = 0; i < terrains.GetLength(0); i++)
                {
                    for (int j = 0; j < terrains.GetLength(1); j++)
                    {
                        var loc = new Location(i, j);
                        if ((loc-spawnLocation).SqrMagnitude < Config.metalSquareRadius)
                            metalLocations.Remove(loc);
                        if ((loc - spawnLocation).SqrMagnitude < Config.waterToMetalMinDistance)
                            waterLocations.Remove(loc);
                    }
                }
                terrains[spawnLocation.x, spawnLocation.y] = ETerrainType.Metal;
                locations.Remove(spawnLocation);
            }
            else
                break;
        }

        for (int index = 0; index < Config.generatorWaterNumber; index++)
        {
            if (waterLocations.Count > 0)
            {
                var spawnLocation = waterLocations[Random.Range(0, waterLocations.Count)];
                for (int i = 0; i < terrains.GetLength(0); i++)
                {
                    for (int j = 0; j < terrains.GetLength(1); j++)
                    {
                        var loc = new Location(i, j);
                        if ((loc - spawnLocation).SqrMagnitude < Config.waterSquareRadius)
                            waterLocations.Remove(loc);
                    }
                }
                terrains[spawnLocation.x, spawnLocation.y] = ETerrainType.Water;
                waterLocations.Remove(spawnLocation);
                locations.Remove(spawnLocation);
            }
            else
                break;
        }

        foreach (var loc in locations)
        {
            int randomizer = Random.Range(0, 8);
            switch (randomizer)
            {
                case 0: terrains[loc.x, loc.y] = ETerrainType.Wasteland; break;
                case 1: terrains[loc.x, loc.y] = ETerrainType.Wasteland; break;
                case 2: terrains[loc.x, loc.y] = ETerrainType.Sand; break;
                case 3: terrains[loc.x, loc.y] = ETerrainType.Sand; break;
                case 4: terrains[loc.x, loc.y] = ETerrainType.Sand; break;
                case 5: terrains[loc.x, loc.y] = ETerrainType.Sand; break;
                default:
                    {
                        terrains[loc.x, loc.y] = ETerrainType.Rock;
                        break;
                    }
            }
        }
        return terrains;

    }
}
