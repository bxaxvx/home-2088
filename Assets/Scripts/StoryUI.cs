﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class StoryUI : MonoBehaviour
{
    [SerializeField]
    private Sprite[] storySprites;
    [SerializeField]
    private TextMeshProUGUI storyText;
    [SerializeField]
    private Image storyImage;

    private Action hideCallback;
    private bool isDisplaying;
            
    public void Display(int imageIndex, string text)
    {
        if (isDisplaying)
        {
            hideCallback = () => Display(imageIndex, text);
        }
        else
        {
            isDisplaying = true;
            storyText.text = text;
            storyImage.sprite = storySprites[Mathf.Clamp(imageIndex, 0, storySprites.Length)];
            gameObject.SetActive(true);
        }
    }

    public void Hide()
    {
        isDisplaying = false;
        gameObject.SetActive(false);
        if (hideCallback != null)
            hideCallback();
        hideCallback = null;
    }

}
