﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Building", menuName = "Building")]
public class BuildingData : ScriptableObject
{
    [Header("Data")]
    public EBuildingType type;
    public List<ETerrainType> allowedTerrain;
    public bool isShadowAllowed;
    public bool isLeveledUp;
    public bool dryWaterAround;

    public int metalCost;
    public int energyCost;
    public int workersCost;

    [Header("Bonus value")]
    public int bonusCapacity;
    public int bonusEnergy;
    public int bonusFoodPerTurn;
    public int bonusWaterPerTurn;
    public int bonusMetalPerTurn;

    [Header("Text")]
    public string title;
    [SerializeField][TextArea]
    private string description;

    [Header("Graphics")]
    public float animationSpeed;
    public Sprite[] disabledBuildingSprites;
    public Sprite[] buildingSpritesTier1;
    public Sprite[] buildingSpritesTier2;
    public Sprite[] buildingSpritesTier3;
    public Sprite shadowSpriteTier1;
    public Sprite shadowSpriteTier2;
    public Sprite shadowSpriteTier3;
    public Sprite[] directionOrientedSprites;

    public Sprite GetBuildingSprite(int level)
    {
        if (type == EBuildingType.WaterPump || type == EBuildingType.Factory)
        {
            if (level >= 3 && buildingSpritesTier3.Length > 0)
                return buildingSpritesTier3[0];
            if (level >= 2 && buildingSpritesTier2.Length > 0)
                return buildingSpritesTier2[0];
        }
        else
        {
            if (level >= 5 && buildingSpritesTier3.Length > 0)
                return buildingSpritesTier3[0];
            if (level >= 3 && buildingSpritesTier2.Length > 0)
                return buildingSpritesTier2[0];
        }
        return buildingSpritesTier1[0];
    }

    public Sprite GetDirectionalSprite(bool hasTop, bool hasRight, bool hasBottom, bool hasLeft)
    {
        string mask = (hasTop ? "1" : "0") + (hasRight ? "1" : "0") + (hasBottom ? "1" : "0") + (hasLeft ? "1" : "0");
        switch (mask)
        {
            case "1111": return directionOrientedSprites[0];
            case "0111": return directionOrientedSprites[7];
            case "1011": return directionOrientedSprites[8];
            case "1101": return directionOrientedSprites[9];
            case "1110": return directionOrientedSprites[10];
            case "1010": return directionOrientedSprites[2];
            case "0101": return directionOrientedSprites[1];
            case "1001": return directionOrientedSprites[3];
            case "1100": return directionOrientedSprites[4];
            case "0110": return directionOrientedSprites[5];
            case "0011": return directionOrientedSprites[6];
            case "1000": return directionOrientedSprites[2];
            case "0010": return directionOrientedSprites[2];
            case "0100": return directionOrientedSprites[1];
            case "0001": return directionOrientedSprites[1];
        }
        return directionOrientedSprites[0];
    }

    public Sprite GetShadowSprite(int level)
    {
        if (level >= 5 && shadowSpriteTier3 != null)
            return shadowSpriteTier3;
        if (level >= 3 && shadowSpriteTier2 != null)
            return shadowSpriteTier2;
        return shadowSpriteTier1;
    }

    public Sprite[] GetBuildingSprites(int level)
    {
        if (type == EBuildingType.WaterPump || type == EBuildingType.Factory)
        {
            if (level >= 3 && buildingSpritesTier3.Length > 0)
                return buildingSpritesTier3;
            if (level >= 2 && buildingSpritesTier2.Length > 0)
                return buildingSpritesTier2;
        }
        else
        {
            if (level >= 5 && buildingSpritesTier3.Length > 0)
                return buildingSpritesTier3;
            if (level >= 3 && buildingSpritesTier2.Length > 0)
                return buildingSpritesTier2;
        }

        return buildingSpritesTier1;
    }

    public string GetDescription()
    {
        return GetDescription(1, true);
    }

    public string GetDescription(int level, bool isEnabled)
    {
        return string.Format(description, isEnabled ?  GetBuildingBonusString(EResourceType.Workers, level) : "0", isEnabled ? GetBuildingBonusString(EResourceType.Energy, level) : "0",
            isEnabled ? GetBuildingBonusString(EResourceType.Food, level) : "0", isEnabled ? GetBuildingBonusString(EResourceType.Water, level) : "0", isEnabled ? GetBuildingBonusString(EResourceType.Metal, level) :"0");
    }

    public int GetBuildingBonus(EResourceType type, int level)
    {
        switch (type)
        {
            case EResourceType.Energy: return (int)(bonusEnergy * GetEfficiency(level));
            case EResourceType.Workers: return (int)(bonusCapacity * GetEfficiency(level));
            case EResourceType.Food: return (int)(bonusFoodPerTurn * GetEfficiency(level));
            case EResourceType.Water: return (int)(bonusWaterPerTurn * GetEfficiency(level));
            case EResourceType.Metal: return (int)(bonusMetalPerTurn * GetEfficiency(level));
        }
        return 0;
    }

    public string GetBuildingBonusString(EResourceType type, int level)
    {
        int value = 0;
        switch (type)
        {
            case EResourceType.Energy: value = (int)(bonusEnergy * GetEfficiency(level)); break;
            case EResourceType.Workers: value = (int)(bonusCapacity * GetEfficiency(level)); break;
            case EResourceType.Food: value = (int)(bonusFoodPerTurn * GetEfficiency(level)); break;
            case EResourceType.Water: value = (int)(bonusWaterPerTurn * GetEfficiency(level)); break;
            case EResourceType.Metal: value = (int)(bonusMetalPerTurn * GetEfficiency(level)); break;
        }
        return string.Format("<b>{0}{1}</b>", value, level == 1 ? "" : "(+" + (value -  value / GetEfficiency(level)) + ")");
    }

    public int GetBuildingLevelUpBonus(EResourceType type, int oldLevel, int newLevel)
    {
        switch (type)
        {
            case EResourceType.Energy: return (int)(bonusEnergy * (GetEfficiency(newLevel) - GetEfficiency(oldLevel)));
            case EResourceType.Workers: return (int)(bonusCapacity * (GetEfficiency(newLevel) - GetEfficiency(oldLevel)));
            case EResourceType.Food: return (int)(bonusFoodPerTurn * (GetEfficiency(newLevel) - GetEfficiency(oldLevel)));
            case EResourceType.Water: return (int)(bonusWaterPerTurn * (GetEfficiency(newLevel) - GetEfficiency(oldLevel)));
            case EResourceType.Metal: return (int)(bonusMetalPerTurn * (GetEfficiency(newLevel) - GetEfficiency(oldLevel)));
        }
        return 0;
    }

    public bool IsBuildPossible(ETerrainType terrainType, bool isInShadow)
    {
        return allowedTerrain.Contains(terrainType) && (!isInShadow || isShadowAllowed);
    }

    public float GetEfficiency(int level)
    {
        level = Mathf.Clamp(level, 0, 5);
        switch (level)
        {
            case 0: return 1f;
            case 1: return 1f;
            case 2: return 1.25f;
            case 3: return 1.5f;
            case 4: return 1.75f;
            case 5: return MaxEfficiency;
        }
        return MaxEfficiency;
    }

    public float MaxEfficiency
    {
        get
        {
            return 2f;
        }
    }
}
