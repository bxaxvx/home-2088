﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Terrain", menuName = "Terrain")]
public class TerrainData : ScriptableObject
{
    public string title;
    public ETerrainType type;
    public string description;

    public Sprite[] terrainSprites;
    public Sprite[] topSprites;
    public Sprite[] shadowSprites;

    public float animationSpeed;
}
