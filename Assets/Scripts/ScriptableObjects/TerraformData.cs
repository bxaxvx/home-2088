﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

[CreateAssetMenu(fileName = "Terraform", menuName = "Terraform")]
public class TerraformData : ScriptableObject
{
    public List<TerraformPatternRow> pattern;
    public ETerraformClass type;

    public ETerraformType GetTerraformType(int rowIndex, int columnIndex)
    {
        Assert.IsTrue(rowIndex >= 0 && rowIndex < pattern.Count && columnIndex >= 0 && columnIndex < pattern[0].elements.Count);
        return pattern[rowIndex].elements[columnIndex];
    }

    public void RotateTerraform(bool clockwise)
    {
        var array = ToArray(pattern);
        if (!clockwise)
        {
            array = array.RotateMatrixCounterClockwise();
            array = array.RotateMatrixCounterClockwise();
            array = array.RotateMatrixCounterClockwise();
        }
        else
            array = array.RotateMatrixCounterClockwise();
        pattern = ToList(array);
    }

    private ETerraformType[,] ToArray(List<TerraformPatternRow> pattern)
    {
        int height = pattern.Count;
        int width = pattern[0].elements.Count;
        ETerraformType[,] array = new ETerraformType[width, height];
        for (int j = 0; j < height; j++)
        {
            for (int i = 0; i < width; i++)
            {
                array[i, j] = GetTerraformType(j, i);
            }
        }
        return array;
    }

    private List<TerraformPatternRow> ToList(ETerraformType[,] array)
    {
        int height = pattern.Count;
        int width = pattern[0].elements.Count;
        List<TerraformPatternRow> list = new List<TerraformPatternRow>();
        for (int j = 0; j < height; j++)
        {
            TerraformPatternRow row = new TerraformPatternRow();
            for (int i = 0; i < width; i++)
            {
                row.elements.Add(array[i, j]);
            }
            list.Add(row);
        }
        return list;
    }
}
