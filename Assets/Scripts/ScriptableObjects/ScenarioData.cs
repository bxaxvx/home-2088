﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Scenario", menuName = "Scenario")]
public class ScenarioData : ScriptableObject
{
    public List<TerraformEvent> terraformEvents;
    public List<PeopleArriveEvent> peopleEvents;
    public List<StoryEvent> storyEvents;

    [SerializeField]
    private int populationGoal;
    [SerializeField]
    private int easyTurnLimit;
    [SerializeField]
    private int mediumTurnLimit;
    [SerializeField]
    private int hardTurnLimit;

    public int PopulationGoal(EDifficulty difficulty)
    {
        if (difficulty == EDifficulty.Sandbox)
            return int.MaxValue;
        else
            return populationGoal;
    }

    public int TurnLimit(EDifficulty difficulty)
    {
        if (difficulty == EDifficulty.Easy)
            return easyTurnLimit;
        else if (difficulty == EDifficulty.Medium)
            return mediumTurnLimit;
        else if (difficulty == EDifficulty.Hard)
            return hardTurnLimit;
        else
            return int.MaxValue;
    }
}
