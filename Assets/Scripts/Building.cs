﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Building : MonoBehaviour
{
    [SerializeField]
    private SpriteRenderer buildingRenderer;
    [SerializeField]
    private SpriteRenderer buildingShadowRenderer;
    [SerializeField]
    private SpriteRenderer wiresRenderer;

    [SerializeField]
    private TextMeshPro uiText;
    [SerializeField]
    private FrameAnimation buildingFrameAnimation;

    private Location location;
    private BuildingData data;
    private int level = 1;
    private bool isTurnedOn;
    private Action onBuildingWasChanged;
    private bool isInShadow;
    private ETerrainType terrainType;
    public bool isLanding;

    public bool HasBuilding
    {
        get
        {
            return data != null;
        }
    }

    public void DisplayBuildingBonus(float efficiency)
    {
        uiText.color = efficiency >= 1f ? Color.green : Color.red;
        uiText.text = efficiency * 100 + "%";
        uiText.enabled = true;
    }

    public void HideBuildingBonus()
    {
        if (isTurnedOn || !HasBuilding)
            uiText.enabled = false;
    }

    public void Build(BuildingData data, Location location, Action onBuildingWasChanged, bool buildForFree = false)
    {
        this.onBuildingWasChanged = onBuildingWasChanged;
        this.location = location;
        this.data = data;
        isTurnedOn = true;
        level = 1;
        GameManager.Instance.PlayData.OnBuildingBuilt(data, level, buildForFree);
        if (data.dryWaterAround)
            DryTerrainAround();
    }

    private void DryTerrainAround()
    {
        var neighbors = location.GetNeighbors(GameManager.Instance.CurrentLevel.Dimension);
        foreach (var neighbor in neighbors)
        {
            var tile = GameManager.Instance.CurrentLevel.GetTile(neighbor);
            if (tile != null && tile.TryDryTerrain(null, Type == EBuildingType.Factory))
            {
                tile.SetTerrain(ETerrainType.Sand);
            }
        }
    }

    private void Rebuild(BuildingData data, Location location, int level)
    {
        this.location = location;
        this.data = data;
        isTurnedOn = true;
        this.level = level;
    }

    public BuildingData Data
    {
        get
        {
            return data;
        }
    }

    public EBuildingType Type
    {
        get
        {
            return data.type;
        }
    }

    public int Level
    {
        get
        {
            return level;
        }
    }

    public bool IsLeveledUp
    {
        get
        {
            return data.isLeveledUp;
        }
    }

    public bool IsEnabled
    {
        get
        {
            return isTurnedOn;
        }
    }


    public void UpdateNeighbors(BuildingData data = null)
    {
        var synergyNeighbors = GameManager.Instance.CurrentLevel.GetNeighborSynergyTiles(location, data == null ? this.data : data);
        foreach (var neighbor in synergyNeighbors)
            neighbor.Building.UpdateLevel();
    }

    public bool UpdateLevel()
    {
        if (HasBuilding)
        {
            var synergyNeighbors = GameManager.Instance.CurrentLevel.GetNeighborSynergyTiles(location, data);
            int oldLevel = level;
            Rebuild(data, location, 1 + synergyNeighbors.Count);
            Display();
            if (oldLevel != level)
            {
                GameManager.Instance.PlayData.OnBuildingLevelChanged(data, oldLevel, level);
                onBuildingWasChanged();
            }
            return oldLevel != level;
        }
        return false;
    }

    public float GetEfficiency(int level)
    {
        if (!isTurnedOn)
            return 0f;
        return data.GetEfficiency(level);
    }

    public float GetEfficiency()
    {
        return GetEfficiency(level);
    }


    public void Disable()
    {
        if (HasBuilding && isTurnedOn)
        {
            GameManager.Instance.PlayData.OnBuildingDisabled(Data, level);
            onBuildingWasChanged();
            isTurnedOn = false;
            DisplayBuildingBonus(0f);
            Display();
            UpdateNeighbors();
            UIManager.Instance.resourcePanelUI.UpdateData(GameManager.Instance.PlayData);
            UIManager.Instance.buildingsPanel.Init(DataManager.Instance.buildingsData);
        }
    }

    public void Enable()
    {
        if (HasBuilding && !isTurnedOn)
        {
            isTurnedOn = true;
            Display();
            onBuildingWasChanged();
            GameManager.Instance.PlayData.OnBuildingEnabled(Data, level);
            UpdateNeighbors();
            UIManager.Instance.resourcePanelUI.UpdateData(GameManager.Instance.PlayData);
            UIManager.Instance.buildingsPanel.Init(DataManager.Instance.buildingsData);
            HideBuildingBonus();
        }
    }

    public void Display(bool isPreview = false)
    {
        Display(Data, isPreview ? 1 : Level, isTurnedOn, isPreview);
    }

    public void Hide()
    {
        buildingRenderer.enabled = false;
        buildingShadowRenderer.enabled = false;
        wiresRenderer.enabled = false;
    }

    public void Display(BuildingData buildingData, int level, bool isOn, bool isPreview = false)
    {
        if (buildingData == null || isLanding)
            Hide();
        else
        {
            buildingRenderer.color = new Color(1f, 1f, 1f, isPreview ? 0.5f : 1f);
            buildingShadowRenderer.color = new Color(1f, 1f, 1f, isPreview ? 0.5f : 1f);
            if (buildingData.type == EBuildingType.PowerLine)
            {
                buildingFrameAnimation.StopLoop();
                buildingRenderer.sprite = GetWireSprite();
                buildingShadowRenderer.sprite = null;
                wiresRenderer.sprite = null;
            }
            else
            {
                if (isOn || buildingData.disabledBuildingSprites.Length == 0)
                    buildingFrameAnimation.PlayLoop(buildingRenderer, buildingData.GetBuildingSprites(level), buildingData.animationSpeed);
                else
                    buildingFrameAnimation.PlayLoop(buildingRenderer, buildingData.disabledBuildingSprites, buildingData.animationSpeed);
                buildingShadowRenderer.sprite = buildingData.GetShadowSprite(level);
                if (isPreview || buildingData.type == EBuildingType.Farm || buildingData.type == EBuildingType.WaterPump)
                    wiresRenderer.sprite = null;
                else
                    wiresRenderer.sprite = GetWireSprite();
            }
            buildingRenderer.sortingLayerName = buildingData.type == EBuildingType.PowerLine ? "Terrain" : "Building";
            buildingRenderer.enabled = true;
            buildingShadowRenderer.enabled = true;
            wiresRenderer.enabled = true;
        }
    }

    private Sprite GetWireSprite()
    {
        BuildingData buildingData = DataManager.Instance.GetBuildingData(EBuildingType.PowerLine);
        List<Tile> neighborTiles = new List<Tile>();
        neighborTiles.Add(GameManager.Instance.CurrentLevel.GetTile(location.GetNeighbor(EDirection.Top)));
        neighborTiles.Add(GameManager.Instance.CurrentLevel.GetTile(location.GetNeighbor(EDirection.Right)));
        neighborTiles.Add(GameManager.Instance.CurrentLevel.GetTile(location.GetNeighbor(EDirection.Bottom)));
        neighborTiles.Add(GameManager.Instance.CurrentLevel.GetTile(location.GetNeighbor(EDirection.Left)));

        List<bool> mask = new List<bool>();
        foreach (var tile in neighborTiles)
            mask.Add(tile != null && tile.HasBuilding);
        return buildingData.GetDirectionalSprite(mask[0], mask[1], mask[2], mask[3]); ;
    }

    public void Deconstruct()
    {
        if (HasBuilding)
        {
            if (IsEnabled)
                GameManager.Instance.PlayData.OnBuildingDisabled(Data, level);
            GameManager.Instance.PlayData.OnBuildingDeconstructed(Data);
            onBuildingWasChanged();
            var oldData = data;
            Build(DataManager.Instance.GetBuildingData(EBuildingType.PowerLine), location, onBuildingWasChanged, true);
            UpdateNeighbors(oldData);
            UIManager.Instance.resourcePanelUI.UpdateData(GameManager.Instance.PlayData);
        }
        else
            Debug.LogError("Trying deconstructing empty tile.");
    }

    public void UpdateOrder(int sortingOrder)
    {
        buildingRenderer.sortingOrder = sortingOrder + 1;
        buildingShadowRenderer.sortingOrder = sortingOrder;
        wiresRenderer.sortingOrder = sortingOrder + 1;
    }

    public void OnTerrainChange(ETerrainType terrainType, bool isInShadow)
    {
        this.terrainType = terrainType;
        this.isInShadow = isInShadow;
        if (HasBuilding)
        {
            if (data.IsBuildPossible(terrainType, isInShadow))
                Enable();
            else
                Disable();
        }
    }
}
