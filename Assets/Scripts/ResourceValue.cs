﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ResourceValue
{
    public ResourceValue(EResourceType resourseType, int value)
    {
        this.resourseType = resourseType;
        this.value = value;
    }

    public EResourceType resourseType;
    public int value;

}
