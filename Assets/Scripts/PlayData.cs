﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PlayData
{
    public bool isBaseEstablished;
    public int houseCapacity = 0;
    public EDifficulty difficulty;
    public int Population
    {
        get
        {
            return population;
        }
    }

    public int FreeHousePlaces
    {
        get
        {
            return houseCapacity - population;
        }
    }

    private int population = 0;

    public void AddPopulation(int number)
    {
        population += number;
        resources[(int)EResourceType.Workers].value += number;
        UIManager.Instance.resourcePanelUI.UpdateData(this);
        UIManager.Instance.buildingsPanel.Init(DataManager.Instance.buildingsData);
        if (Population >= GameManager.Instance.scenarioData.PopulationGoal(GameManager.Instance.PlayData.difficulty))
            GameManager.Instance.Victory();
    }

    public int turnNumber = 1;

    private List<ResourceValue> resources = new List<ResourceValue>();
    private List<ResourceValue> resourcesIncome = new List<ResourceValue>();

    private PlayData() { }

    public PlayData(EDifficulty difficulty)
    {
        this.difficulty = difficulty;
        resources.Add(new ResourceValue(EResourceType.Metal, Config.startMetal));
        resources.Add(new ResourceValue(EResourceType.Food, Config.startFood));
        resources.Add(new ResourceValue(EResourceType.Energy, 0));
        resources.Add(new ResourceValue(EResourceType.Water, Config.startWater));
        resources.Add(new ResourceValue(EResourceType.Workers, 0));

        resourcesIncome.Add(new ResourceValue(EResourceType.Metal, 0));
        resourcesIncome.Add(new ResourceValue(EResourceType.Food, 0));
        resourcesIncome.Add(new ResourceValue(EResourceType.Energy, 0));
        resourcesIncome.Add(new ResourceValue(EResourceType.Water, 0));
        resourcesIncome.Add(new ResourceValue(EResourceType.Workers, 0));
    }

    public void CalculateTurn()
    {
        turnNumber++;
        var resEnums = Enum.GetValues(typeof(EResourceType)).Cast<EResourceType>();
        foreach (var res in resEnums)
            resources[(int)res].value += GetIncome(res);
        UIManager.Instance.buildingsPanel.Init(DataManager.Instance.buildingsData);
    }

    public int GetResourceValue(EResourceType resourceType)
    {
        return resources[(int)resourceType].value;
    }

    public int GetIncome(EResourceType resourceType)
    {
        if (resourceType == EResourceType.Water || resourceType == EResourceType.Food)
        {
            return resourcesIncome[(int)resourceType].value - Population;
        }
        else
            return resourcesIncome[(int)resourceType].value;
    }

    public void OnBuildingBuilt(BuildingData buildingData, int level, bool buildForFree = false)
    {
        resources[(int)EResourceType.Energy].value += buildForFree ? 0 : -buildingData.energyCost + buildingData.GetBuildingBonus(EResourceType.Energy, level);
        resources[(int)EResourceType.Metal].value += buildForFree ? 0 : -buildingData.metalCost;
        resources[(int)EResourceType.Workers].value += buildForFree ? 0 : - buildingData.workersCost;
        houseCapacity += buildingData.GetBuildingBonus(EResourceType.Workers, level);
        resourcesIncome[(int)EResourceType.Food].value += buildingData.GetBuildingBonus(EResourceType.Food, level);
        resourcesIncome[(int)EResourceType.Metal].value += buildingData.GetBuildingBonus(EResourceType.Metal, level);
        resourcesIncome[(int)EResourceType.Water].value += buildingData.GetBuildingBonus(EResourceType.Water, level);
        if (buildingData.type == EBuildingType.Base)
            isBaseEstablished = true;
        UIManager.Instance.buildingsPanel.Init(DataManager.Instance.buildingsData);
    }

    public void OnBuildingDisabled(BuildingData buildingData, int level)
    {
        resources[(int)EResourceType.Energy].value -=buildingData.GetBuildingBonus(EResourceType.Energy, level);
        houseCapacity -= buildingData.GetBuildingBonus(EResourceType.Workers, level);
        resourcesIncome[(int)EResourceType.Food].value -= buildingData.GetBuildingBonus(EResourceType.Food, level);
        resourcesIncome[(int)EResourceType.Metal].value -= buildingData.GetBuildingBonus(EResourceType.Metal, level);
        resourcesIncome[(int)EResourceType.Water].value -= buildingData.GetBuildingBonus(EResourceType.Water, level);
        if (buildingData.type == EBuildingType.Base)
            isBaseEstablished = false;
    }

    public void OnBuildingEnabled(BuildingData buildingData, int level)
    {
        resources[(int)EResourceType.Energy].value += buildingData.GetBuildingBonus(EResourceType.Energy, level);
        houseCapacity += buildingData.GetBuildingBonus(EResourceType.Workers, level);
        resourcesIncome[(int)EResourceType.Food].value += buildingData.GetBuildingBonus(EResourceType.Food, level);
        resourcesIncome[(int)EResourceType.Metal].value += buildingData.GetBuildingBonus(EResourceType.Metal, level);
        resourcesIncome[(int)EResourceType.Water].value += buildingData.GetBuildingBonus(EResourceType.Water, level);
        if (buildingData.type == EBuildingType.Base)
            isBaseEstablished = true;
    }

    public void OnBuildingDeconstructed(BuildingData buildingData)
    {
        resources[(int)EResourceType.Energy].value += buildingData.energyCost;
        resources[(int)EResourceType.Workers].value += buildingData.workersCost;
    }

    public void OnBuildingLevelChanged(BuildingData buildingData, int oldLevel, int newLevel)
    {
        resources[(int)EResourceType.Energy].value += buildingData.GetBuildingLevelUpBonus(EResourceType.Energy, oldLevel, newLevel);
        houseCapacity += buildingData.GetBuildingLevelUpBonus(EResourceType.Workers, oldLevel, newLevel);
        resourcesIncome[(int)EResourceType.Food].value += buildingData.GetBuildingLevelUpBonus(EResourceType.Food, oldLevel, newLevel);
        resourcesIncome[(int)EResourceType.Metal].value += buildingData.GetBuildingLevelUpBonus(EResourceType.Metal, oldLevel, newLevel);
        resourcesIncome[(int)EResourceType.Water].value += buildingData.GetBuildingLevelUpBonus(EResourceType.Water, oldLevel, newLevel);
    }

    public bool IsEnoughResources(BuildingData buildingData)
    {
        return buildingData.metalCost <= resources[(int)EResourceType.Metal].value && (buildingData.energyCost <= resources[(int)EResourceType.Energy].value || buildingData.energyCost ==0) && buildingData.workersCost <= GetResourceValue(EResourceType.Workers);
    }
}
