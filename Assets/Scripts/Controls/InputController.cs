﻿using UnityEngine;
using System;
using System.Collections.Generic;

public class InputController : MonoBehaviour
{
    public static InputController Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<InputController>();
            }

            return instance;
        }
    }

    private static InputController instance;

    private List<ClickAction> clickActions = new List<ClickAction>();

    private class ClickAction
    {
        public Vector3 clickPosition;
        public Action onPointerStayedCallback;
        public Action onPointerMovedCallback;

        public ClickAction(Vector3 clickPosition, Action onPointerStayedCallback, Action onPointerMovedCallback)
        {
            this.clickPosition = clickPosition;
            this.onPointerStayedCallback = onPointerStayedCallback;
            this.onPointerMovedCallback = onPointerMovedCallback;
        }
    }

    private List<DragAction> dragActions = new List<DragAction>();

    private class DragAction
    {
        public Action<Vector3> onDragCallback;
        public Action<Vector3> onDragExitCallback;

        public DragAction(Action<Vector3> onDragCallback, Action<Vector3> onDragExitCallback)
        {
            this.onDragCallback = onDragCallback;
            this.onDragExitCallback = onDragExitCallback;
        }
    }

    private List<ZoomAction> zoomActions = new List<ZoomAction>();

    private class ZoomAction
    {
        public Action<float> onZoomCallback;
        public Action onZoomExitCallback;

        public ZoomAction(Action<float> onZoomCallback, Action onZoomExitCallback)
        {
            this.onZoomCallback = onZoomCallback;
            this.onZoomExitCallback = onZoomExitCallback;
        }
    }

    #region Mono behaviour

    private bool isDragging;
    private bool isZooming;
    private float previousZoomValue;
    private Vector3 dragMousePosition;
    private int allowedTouchOffset = 0;

    private void Start()
    {     
        allowedTouchOffset = (int)(Screen.dpi == 0 ? (Screen.height + Screen.width) / 100f : Screen.dpi / 10f);
        allowedTouchOffset = allowedTouchOffset * allowedTouchOffset;
    }

    void Update()
    {
#if UNITY_EDITOR || UNITY_STANDALONE || UNITY_WEBGL

        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
            if (hit.transform != null)
            {
                hit.transform.gameObject.SendMessage("OnClick");
            }

            if (!isDragging && !UIManager.IsPointerOverUI)
            {
                dragMousePosition = UIManager.MouseToWorldPosition;
                isDragging = true;
            }
        }

        if (!UIManager.IsPointerOverUI)
        {
            if (Input.mouseScrollDelta.y != 0)
            {
                foreach (var zoomAction in zoomActions)
                {
                    zoomAction.onZoomCallback(Input.mouseScrollDelta.y);
                }
            }
            if (Input.GetKeyDown(KeyCode.Minus))
            {
                foreach (var zoomAction in zoomActions)
                {
                    zoomAction.onZoomCallback(-1);
                }
            }
            if (Input.GetKeyDown(KeyCode.Equals))
            {
                foreach (var zoomAction in zoomActions)
                {
                    zoomAction.onZoomCallback(1);
                }
            }
        }

        if (Input.GetMouseButtonUp(0))
        {
            Vector3 upMousePosition = Input.mousePosition;

            if (isDragging)
            {
                isDragging = false;
                foreach (var dragAction in dragActions)
                {
                    if (dragAction.onDragExitCallback != null)
                        dragAction.onDragExitCallback(UIManager.MouseToWorldPosition);
                }
            }

            foreach (var clickAction in clickActions)
            {
                if ((clickAction.clickPosition - upMousePosition).sqrMagnitude < allowedTouchOffset && clickAction.onPointerStayedCallback != null)
                    clickAction.onPointerStayedCallback();
                else if (clickAction.onPointerMovedCallback != null)
                    clickAction.onPointerMovedCallback();
            }

            clickActions.Clear();
        }

        if (isDragging)
        {
            foreach (var dragAction in dragActions)
                dragAction.onDragCallback(UIManager.MouseToWorldPosition - dragMousePosition);
            dragMousePosition = UIManager.MouseToWorldPosition;
        }

#elif UNITY_IPHONE || UNITY_ANDROID

        if (Input.touchCount == 1)
        {
            if (Input.GetTouch(0).phase.Equals(TouchPhase.Began))
            {
                RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position), Vector2.zero);
                if (hit.transform != null)
                {
                    hit.transform.gameObject.SendMessage("OnClick");
                }

                if (!isDragging && !UIManager.IsPointerOverUI)
                {
                    dragMousePosition = UIManager.MouseToWorldPosition;
                    isDragging = true;
                }
            }
            else if (Input.GetTouch(0).phase.Equals(TouchPhase.Ended))
            {
                Vector3 upMousePosition = Input.mousePosition;

                if (isDragging)
                {
                    isDragging = false;
                    foreach (var dragAction in dragActions)
                    {
                        if (dragAction.onDragExitCallback != null)
                            dragAction.onDragExitCallback(UIManager.MouseToWorldPosition);
                    }
                }

                foreach (var clickAction in clickActions)
                {
                    if ((clickAction.clickPosition - upMousePosition).sqrMagnitude < allowedTouchOffset && clickAction.onPointerStayedCallback != null)
                        clickAction.onPointerStayedCallback();
                    else if (clickAction.onPointerMovedCallback != null)
                        clickAction.onPointerMovedCallback();
                }

                clickActions.Clear();
            }
        }

        if (Input.touchCount == 2)
        {                        
            clickActions.Clear();
            if (isZooming)
            {
                foreach (var zoomAction in zoomActions)
                {
                    zoomAction.onZoomCallback((Input.GetTouch(0).position - Input.GetTouch(1).position).magnitude - previousZoomValue);
                }
            }
            else if (!UIManager.IsPointerOverUI)
            {            
                isDragging = false;
                isZooming = true;
            }
            previousZoomValue = (Input.GetTouch(0).position - Input.GetTouch(1).position).magnitude;
        }
        else if (isZooming)
        {
            foreach (var zoomAction in zoomActions)
            {
                zoomAction.onZoomExitCallback();
            }
            isZooming = false;
        }

        if (isDragging)
        {
            foreach (var dragAction in dragActions)
                dragAction.onDragCallback(UIManager.MouseToWorldPosition - dragMousePosition);
            dragMousePosition = UIManager.MouseToWorldPosition;
        }
#endif
    }

    #endregion

    public void RegisterOnClickAction(Action onPointerStayedCallback, Action onPointerMovedCallback = null)
    {
        if (!UIManager.IsPointerOverUI)
            clickActions.Add(new ClickAction(Input.mousePosition, onPointerStayedCallback, onPointerMovedCallback));
    }

    public void RegisterOnDragAction(Action<Vector3> onDragCallback, Action<Vector3> onDragExitCallback = null)
    {
        dragActions.Add(new DragAction(onDragCallback, onDragExitCallback));
    }

    public void RegisterOnZoomAction(Action<float> onZoomCallback, Action onZoomExitCallback = null)
    {
        zoomActions.Add(new ZoomAction(onZoomCallback, onZoomExitCallback));
    }
}
