﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileInputHandler : MonoBehaviour
{
    private const float MAX_SQUARE_DISTANCE_FOR_FAST_CLICK = 100f;
    private const float FAST_CLICK_TIME = 0.2f;
    private Location currentTile = new Location(int.MinValue, int.MinValue);

    private Location selectedTile = new Location(int.MinValue, int.MinValue);
    private bool isTileSelected;
    private float timeFromLastClick;

    private bool clickWasMade;
    private Vector3 clickPosition;

    private void Update()
    {
        if (clickWasMade)
        {
            if (timeFromLastClick > FAST_CLICK_TIME)
                clickWasMade = false;
            timeFromLastClick += Time.deltaTime;
        }
        if (UIManager.IsPointerOverUI)
            return;
        if (Input.GetKeyDown(KeyCode.Escape))
            UIManager.Instance.modalDialog.Show("You sure?", "Do you want to quit the game?", "Yes", GameManager.Instance.RestartGame, "No", () =>{ });

        Location newTile = WorldpositionToTile(UIManager.MouseToWorldPosition);
        if (newTile != currentTile)
        {
            OnPointerExit(currentTile);
            currentTile = newTile;
            OnPointerEnter(newTile);
        }
        if (Input.GetMouseButtonDown(0))
            OnPointerClick(newTile);
        if (clickWasMade && Input.GetMouseButtonUp(0))
            OnFastClick(newTile);
        if (Input.GetMouseButtonDown(1))
            OnPointerRightClick(newTile);
        if (GameManager.Instance.SelectedTerraform != null)
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                bool isTerraformPossible;
                GameManager.Instance.SelectedTerraform.RotateTerraform(true);
                UIManager.Instance.UpdateTerraformOptions();
                GameManager.Instance.DisplayTerraform(newTile, out isTerraformPossible);
            }
            else if (Input.GetKeyDown(KeyCode.Q))
            {
                bool isTerraformPossible;
                GameManager.Instance.SelectedTerraform.RotateTerraform(false);
                UIManager.Instance.UpdateTerraformOptions();
                GameManager.Instance.DisplayTerraform(newTile, out isTerraformPossible);
            }
        }
    }

    private void OnPointerRightClick(Location location)
    {
        if (GameManager.Instance.SelectedTerraform != null)
        {
            UIManager.Instance.patternsToggleGroup.SetAllTogglesOff();
            GameManager.Instance.ResetTilesUI();
        }
        else if (GameManager.Instance.IsBuildingMode)
        {
            UIManager.Instance.buildingsPanel.Reset();
            ResetCurrentTile();
        }
        else
        {
            UIManager.Instance.buildingsPanel.Reset(false);
            DeselectTile();
        }
    }

    private void OnFastClick(Location location)
    {
        clickWasMade = false;
        if ((clickPosition - Input.mousePosition).sqrMagnitude > MAX_SQUARE_DISTANCE_FOR_FAST_CLICK)      
            return;
        //if (!GameManager.Instance.IsBuildingMode && GameManager.Instance.SelectedTerraform == null)        
        TrySelectTile(location);        
    }

    private void OnPointerClick(Location location)
    {
        timeFromLastClick = 0f;
        clickPosition = Input.mousePosition;
        clickWasMade = true;

    }

    public void DeselectTile()
    {
        if (isTileSelected)
        {
            GameManager.Instance.CurrentLevel.GetTile(selectedTile).OnDeselect();
            isTileSelected = false;
        }
    }

    private void TrySelectTile(Location location)
    {
        var tile = GameManager.Instance.CurrentLevel.GetTile(location);

        if (tile != null)
        {
            if (!GameManager.Instance.IsBuildingMode && GameManager.Instance.SelectedTerraform == null)
            {
                if (selectedTile != location)
                    DeselectTile();
                selectedTile = location;
                isTileSelected = true;
                tile.OnSelect();
            }

            if (GameManager.Instance.IsBuildingMode)
            {
                if (tile.HasBuilding && GameManager.Instance.isDemolishing)
                    tile.TryDemolish();
                if (!GameManager.Instance.isDemolishing)
                    tile.TryBuildABuilding(GameManager.Instance.SelectedBuilding);
                ResetCurrentTile();
            }
            else if (GameManager.Instance.SelectedTerraform != null)
                GameManager.Instance.TryTerraform(location);
        }        
    }

    private void OnPointerEnter(Location location)
    {
        if (GameManager.Instance.CurrentLevel == null)
            return;
        var tile = GameManager.Instance.CurrentLevel.GetTile(location);
        if (tile != null)
        {
            if (GameManager.Instance.SelectedTerraform != null)
            {
                bool isTerraformPossible;
                GameManager.Instance.DisplayTerraform(location, out isTerraformPossible);
            }
            else
                tile.OnPointerEnter();
        }
    }

    private void OnPointerExit(Location location)
    {
        if (GameManager.Instance.CurrentLevel == null)
            return;
        var tile = GameManager.Instance.CurrentLevel.GetTile(location);
        if (tile != null)
        {
            if (GameManager.Instance.SelectedTerraform != null)
            {
                GameManager.Instance.ResetTilesUI();
            }
            else
                tile.OnPointerExit();
        }
    }

    private Location WorldpositionToTile(Vector3 mouseToWorldPosition)
    {
        return new Location(((int)mouseToWorldPosition.x + Tile.TILE_WIDTH) / Tile.TILE_WIDTH - 1, ((int)mouseToWorldPosition.y + Tile.TILE_HEIGHT) / Tile.TILE_HEIGHT - 1);
    }

    public void ResetCurrentTile()
    {
        OnPointerExit(currentTile);
        OnPointerEnter(currentTile);
    }
}
