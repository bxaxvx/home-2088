﻿using UnityEngine;
using System;
//using UnityStandardAssets.ImageEffects;
//using EZCameraShake;
using System.Collections.Generic;

public class CameraBehavior : MonoBehaviour
{
    [SerializeField]
    private Camera cameraComponent;
    [SerializeField]
    private Transform movingHolderTransform;
    [SerializeField]
    private LeanTweenType cameraZoomEasyType;
    [SerializeField]
    private float moveSpeed = 100f;

    private const float SCROLL_MODIFIER = 500f;

    //private Vector3 minBound;
    //private Vector3 maxBound;
    //private Vector3 boundsDelta /*= new Vector3(LevelGenerator.ROOM_STEP_X, LevelGenerator.ROOM_STEP_Y)*/;

    public const int PIXELS_PER_UNIT = 1;
    private int zoomValue = 1;
    private bool isLocked = false;
    private bool isZooming;

    #region Mono behaviour

    private float OrthographicSize
    {
        get
        {
            return cameraComponent.orthographicSize; 
        }
        set
        {
            cameraComponent.orthographicSize = value;
        }
    }


    private void Start()
    {
        InputController.Instance.RegisterOnDragAction(Drag);
        InputController.Instance.RegisterOnZoomAction(OnZoom, FinishZoom);
        CalculateCameraSizes();
        //SetZoomIndex(1);
    }

    private void Update()
    {        
        if (!UIManager.Instance.modalDialog.isActiveAndEnabled)
            movingHolderTransform.Translate(new Vector3(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical")).normalized * moveSpeed * Time.deltaTime);
        //movingHolderTransform.position = new Vector3((int)movingHolderTransform.position.x, (int)movingHolderTransform.position.y, movingHolderTransform.position.z);
    }

    private void LateUpdate()
    {
    }

    #endregion

#if UNITY_ANDROID
    public const float DEFAULT_VIGNETTE_VALUE = 0f;
#else
    public const float DEFAULT_VIGNETTE_VALUE = 0.1f;
#endif

    private void Drag(Vector3 deltaPosition)
    {
        movingHolderTransform.position -= deltaPosition;
        //movingHolderTransform.position = new Vector3(Mathf.Clamp(movingHolderTransform.position.x, minBound.x - boundsDelta.x, maxBound.x + boundsDelta.x), Mathf.Clamp(movingHolderTransform.position.y, minBound.y - boundsDelta.y, maxBound.y + boundsDelta.y), -10);
    }

    private int previousZoomIndex;

    public void Lock()
    {
        isLocked = true;
    }

    public void Unlock()
    {
        isLocked = false;
    }

    private void OnZoom(float deltaZoom)
    {
        if (isLocked || isZooming)
            return;
#if UNITY_EDITOR || UNITY_STANDALONE || UNITY_WEBGL
        if (deltaZoom > 0)
            ZoomIn();
        else
            ZoomOut();
#elif UNITY_IPHONE || UNITY_ANDROID
        cameraComponent.orthographicSize = Mathf.Clamp(
            cameraComponent.orthographicSize - deltaZoom * ((1280f * 720f) / ((float)(Screen.width * Screen.height))),
            cameraSizes[cameraSizes.Count - 1] - 30,
            cameraSizes[0] + 30   
            );
#endif
    }

    private void ZoomIn()
    {
        SetZoomIndex(Mathf.Clamp(zoomValue + 1, 0, ZoomLevelsNum - 1), false);
    }

    private void ZoomOut()
    {
        SetZoomIndex(Mathf.Clamp(zoomValue - 1, 0, ZoomLevelsNum - 1), false);
    }

    private void FinishZoom()
    {
        SetZoomIndex(cameraSizes.FindClosestNumberIndexInList(OrthographicSize), true, 0.2f);
    }

    private void SetZoomIndex(int zoomIndex, bool softZoom = false, float zoomTime = 0.7f, Action callback = null)
    {
        zoomValue = Mathf.Clamp(zoomIndex, 0, cameraSizes.Count - 1);
        CalculateCameraSizes();
        SetZoom(cameraSizes[zoomValue], softZoom, zoomTime, callback);
    }

    public void SetHardZoom(int zoomIndex)
    {
        SetZoomIndex(zoomIndex, false);
    }

    public void SetSoftZoom(int zoomIndex)
    {
        SetZoomIndex(zoomIndex, true, 0.5f);
    }

    public void SetSlowSoftZoom(int zoomIndex)
    {
        SetZoomIndex(zoomIndex, true, 1f);
    }

    private void SetZoom(float cameraSize, bool softZoom = false, float zoomTime = 0.7f, Action callback = null)
    {
        if (isLocked)
        {
            if (callback != null)
                callback();
            return;
        }
        if (!softZoom)
            OrthographicSize = cameraSize;
        else
        {
            isZooming = true;
            LeanTween.value(OrthographicSize, cameraSize, zoomTime).setOnUpdate(SetCameraSize).setOnComplete(callback).setEase(cameraZoomEasyType);
        }
    }

    private void SetCameraSize(float size)
    {
        OrthographicSize = size;
    }

    private const float MAX_ZOOM_PORTRAIT = 180;
    private const float MIN_ZOOM_PORTRAIT = 700;
    private const float BATTLE_ZOOM_PORTRAIT = 120;

    private const float MAX_ZOOM_LANDSCAPE = 70;
    private const float MIN_ZOOM_LANDSCAPE = 700;
    private const float BATTLE_ZOOM_LANDSCAPE = 70;

    private const float MAX_ZOOM_DESKTOP = 70;
    private const float MIN_ZOOM_DESKTOP = 700;
    private const float BATTLE_ZOOM_DESKTOP = 70;

    private List<float> cameraSizes = new List<float>();
    private float battleCameraSize;

    private int ZoomLevelsNum
    {
        get
        {
            return cameraSizes.Count;
        }
    }

    private float calculatedScreenHeight = 0f;
    private void CalculateCameraSizes()
    {
        if (Screen.height == calculatedScreenHeight)
            return;
        calculatedScreenHeight = Screen.height;
        cameraSizes.Clear();
        int i = 0;
        while (true)
        {
            i++;
            float cameraSize = Screen.height / (i * PIXELS_PER_UNIT * 2);
            if (cameraSize <= MinZoom && cameraSize >= MaxZoom)
                cameraSizes.Add(cameraSize);
            if (cameraSize < MaxZoom)
                break;
        }
        i = 0;
        while (true)
        {
            i++;
            float cameraSize = Screen.height / (i * PIXELS_PER_UNIT * 2);
            float deltaTargetZoom = cameraSize - BattleZoom;
            if (deltaTargetZoom < 0)
                break;
            else
                battleCameraSize = cameraSize;
        }

    }

    private float MaxZoom
    {
        get
        {
#if UNITY_ANDROID || UNITY_IPHONE
            if (Screen.width > Screen.height)
                return MAX_ZOOM_LANDSCAPE;
            else
                return MAX_ZOOM_PORTRAIT;
#else
            return MAX_ZOOM_DESKTOP;
#endif
        }
    }

    private float MinZoom
    {
        get
        {
#if UNITY_ANDROID || UNITY_IPHONE
            if (Screen.width > Screen.height)
                return MIN_ZOOM_LANDSCAPE;
            else
                return MIN_ZOOM_PORTRAIT;
#else
            return MIN_ZOOM_DESKTOP;
#endif
        }
    }

    private float BattleZoom
    {
        get
        {
#if UNITY_ANDROID || UNITY_IPHONE
            if (Screen.width > Screen.height)
                return BATTLE_ZOOM_LANDSCAPE;
            else
                return BATTLE_ZOOM_PORTRAIT;
#else
            return BATTLE_ZOOM_DESKTOP;
#endif
        }
    }
}

