﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;

public class ResourcePanelUI : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI[] resourcesText;

    [SerializeField]
    private TextMeshProUGUI houseText;
    [SerializeField]
    private TextMeshProUGUI timeText;

    public void UpdateData(PlayData playData)
    {
        timeText.text = playData.turnNumber.ToString();
        houseText.text = playData.Population + "/" + playData.houseCapacity;
        var resEnums = Enum.GetValues(typeof(EResourceType)).Cast<EResourceType>();
        foreach (var res in resEnums)
        {
            string income = "";
            if (res != EResourceType.Energy && res != EResourceType.Workers)
            {
                var value = playData.GetIncome(res);
                income = (value >= 0 ? "+" : "") + value;
                income = "(<color=\"" + ((value > 0) ? "green" : (value == 0 ? "white" : "red")) + "\">" + income + "<color=\"white\">)";
            }
            resourcesText[(int)res].text = playData.GetResourceValue(res) + income;
        }
    }
}
