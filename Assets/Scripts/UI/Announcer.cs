﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Announcer : MonoBehaviour
{
    [SerializeField]
    private AnnounceUI populationAnnounceUI;
    [SerializeField]
    private AnnounceUI terraformAnnounceUI;

    public void OnTurnMade(int turnNumber, ScenarioData scenarioData)
    {
        bool isInit = false;
        foreach (var peopleEvent in scenarioData.peopleEvents)
        {
            int clampedTurnNumber = turnNumber % (scenarioData.peopleEvents[scenarioData.peopleEvents.Count - 1].turnNumber + 1);
            if (peopleEvent.turnNumber <= clampedTurnNumber)
                continue;
            isInit = true;
            populationAnnounceUI.Init(peopleEvent, peopleEvent.turnNumber - clampedTurnNumber);
            populationAnnounceUI.gameObject.SetActive(true);
            break;
        }
        if (!isInit)
            populationAnnounceUI.gameObject.SetActive(false);

        isInit = false;
        foreach (var terraformEvent in scenarioData.terraformEvents)
        {
            int clampedTurnNumber = turnNumber % (scenarioData.terraformEvents[scenarioData.terraformEvents.Count - 1].turnNumber + 1);
            if (terraformEvent.turnNumber <= clampedTurnNumber)
                continue;
            isInit = true;
            terraformAnnounceUI.Init(terraformEvent, terraformEvent.turnNumber - clampedTurnNumber);
            terraformAnnounceUI.gameObject.SetActive(true);
            break;
        }
        if (!isInit)
            terraformAnnounceUI.gameObject.SetActive(false);
    }
}
