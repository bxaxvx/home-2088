﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TerraformTileUI : MonoBehaviour
{
    [SerializeField]
    private Image backgroundImage;
    [SerializeField]
    private Image iconImage;

    public void Init(ETerraformType terraformType, bool isOdd)
    {
        if (terraformType == ETerraformType.None)
            iconImage.enabled = false;
        else
        {
            iconImage.sprite = DataManager.Instance.terraformIcons.GetSprite(terraformType.ToString());
            iconImage.enabled = true;
        }
        backgroundImage.color = new Color(1f, 1f, 1f, isOdd ? 60f / 255f : 40f / 255f);
    }
}
