﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

public class PatternDisplayUI : MonoBehaviour
{
    [SerializeField]
    private GameObject terraformTileUIPrefab;
    [SerializeField]
    private GridLayoutGroup gridLayout;
    [SerializeField]
    private Toggle toggle;

    private TerraformData data;
    List<TerraformTileUI> tilesUI = new List<TerraformTileUI>();

    public void Init(TerraformData terraformData, ToggleGroup toggleGroup)
    {
        data = terraformData;
        toggle.group = toggleGroup;
        gridLayout.constraintCount = terraformData.pattern.Count;
        int height = terraformData.pattern.Count;
        int width = terraformData.pattern[0].elements.Count;
        int elementsNum = width * height;
        Assert.IsTrue(elementsNum > 0);
        HelperTools.NormalizePool(ref tilesUI, terraformTileUIPrefab, transform, elementsNum);
        for (int j = 0; j < height; j++)
        {
            for (int i = 0; i < width; i++)
            {
                int index = j * width + i;
                tilesUI[index].Init(terraformData.GetTerraformType(j, i), (index + ((width % 2 == 0 && j % 2 == 1) ? 1 : 0)) % 2 == 0);
            }
        }
    }

    public void OnToggleSelect(bool isActive)
    {
        UIManager.Instance.controlsText.enabled = isActive;
        if (isActive)
            GameManager.Instance.OnTerraformSelected(data);
        else
            GameManager.Instance.OnTerraformDeselected(data);
    }
}
