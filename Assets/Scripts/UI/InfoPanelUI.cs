﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class InfoPanelUI : MonoBehaviour
{
    [SerializeField]
    private Image mainImage;
    [SerializeField]
    private TextMeshProUGUI titleText;

    [SerializeField]
    private GameObject costHolder;
    [SerializeField]
    private GameObject metalHolder;
    [SerializeField]
    private TextMeshProUGUI metalText;
    [SerializeField]
    private GameObject energyHolder;
    [SerializeField]
    private TextMeshProUGUI energyText;
    [SerializeField]
    private GameObject workersHolder;
    [SerializeField]
    private TextMeshProUGUI workersText;

    [SerializeField]
    private Transform terrainsHolder;
    [SerializeField]
    private GameObject tilePrefab;

    [SerializeField]
    private TextMeshProUGUI efficiencyText;
    [SerializeField]
    private GameObject shadowText;

    [SerializeField]
    private TextMeshProUGUI descriptionText;

    private List<Image> terrainImages = new List<Image>();


    public void Init(Tile tile)
    {
        var terrainData = DataManager.Instance.GetTerrainData(tile.TerrainType);
        titleText.text = terrainData.title;
        costHolder.SetActive(false);
        shadowText.SetActive(false);
        descriptionText.text = terrainData.description;
        mainImage.sprite = tile.TerrainType == ETerrainType.Rock ? terrainData.topSprites[0] : terrainData.terrainSprites[0];
        efficiencyText.text = "<color=red>In the shadow.</color>";
        efficiencyText.alignment = TextAlignmentOptions.Right;
        efficiencyText.gameObject.SetActive(true && tile.IsInShadow);
        gameObject.SetActive(true);
    }

    public void Init(Building building)
    {
        titleText.text = building.Data.title;
        costHolder.SetActive(false);
        shadowText.SetActive(false);
        efficiencyText.alignment = TextAlignmentOptions.Left;
        descriptionText.text = building.Data.GetDescription(building.Level, building.IsEnabled);
        efficiencyText.text = "Efficiency:" + (building.IsEnabled ? building.Data.GetEfficiency(building.Level) * 100f : 0f).FormatValue(100f, "%");
        mainImage.sprite = building.Data.GetBuildingSprite(building.Level);
        efficiencyText.gameObject.SetActive(true && building.Data.isLeveledUp);
        gameObject.SetActive(true);
    }

    public void Init(BuildingData data)
    {
        titleText.text = data.title;
        costHolder.SetActive(true);
        efficiencyText.gameObject.SetActive(false);

        metalHolder.SetActive(data.metalCost > 0);
        bool isNotEnough = GameManager.Instance.PlayData.GetResourceValue(EResourceType.Metal) < data.metalCost;
        metalText.text = "<color=\"" + (isNotEnough ? "red\">" : "white\">") + data.metalCost.ToString();

        energyHolder.SetActive(data.energyCost > 0);
        isNotEnough = GameManager.Instance.PlayData.GetResourceValue(EResourceType.Energy) < data.energyCost;
        energyText.text = "<color=\"" + (isNotEnough ? "red\">" : "white\">") + data.energyCost.ToString();

        workersHolder.SetActive(data.workersCost > 0);
        isNotEnough = GameManager.Instance.PlayData.GetResourceValue(EResourceType.Workers) < data.workersCost;
        workersText.text = "<color=\"" + (isNotEnough ? "red\">" : "white\">") + data.workersCost.ToString();

        mainImage.sprite = data.GetBuildingSprite(1);
        HelperTools.NormalizePool(ref terrainImages, tilePrefab, terrainsHolder, data.allowedTerrain.Count);
        for (int i = 0; i < data.allowedTerrain.Count; i++)        
            terrainImages[i].sprite = DataManager.Instance.GetTerrainSprite(data.allowedTerrain[i]);      
        shadowText.SetActive(!data.isShadowAllowed);
        descriptionText.text = data.GetDescription();
        gameObject.SetActive(true);
    }

    public void InitDemolish(Sprite icon)
    {
        titleText.text = "Demolish";
        efficiencyText.gameObject.SetActive(false);
        mainImage.sprite = icon;
        descriptionText.text = "Deconstruct building.";
        shadowText.SetActive(false);
        costHolder.SetActive(false);
        gameObject.SetActive(true);
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }

}
