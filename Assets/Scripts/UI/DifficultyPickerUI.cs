﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DifficultyPickerUI : MonoBehaviour
{
    [SerializeField]
    private ObjectiveUI objectiveUI;

    private EDifficulty difficulty;
    public EDifficulty Difficulty
    {
        get { return difficulty; }
    }


    private void Start()
    {
        SetDifficulty(2);
    }

    public void SetDifficulty(int diff)
    {
        difficulty = (EDifficulty)diff;
        if (difficulty == EDifficulty.Sandbox)
            objectiveUI.Hide();
        else
            objectiveUI.Init(GameManager.Instance.scenarioData.PopulationGoal(difficulty), GameManager.Instance.scenarioData.TurnLimit(difficulty));
    }
}
