﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuildingsPanelUI : MonoBehaviour
{
    [SerializeField]
    private GameObject buildingButtonPrefab;    
    [SerializeField]
    private ToggleGroup toggleGroup;
    [SerializeField]
    private BuildingButton demolishButton;

    private List<BuildingButton> buildingButtons = new List<BuildingButton>();
    private bool isDisplaying;

    public void Reset(bool reenableDisplay = true)
    {
        if (!isDisplaying && reenableDisplay)
            ToggleDisplay();
        toggleGroup.SetAllTogglesOff();
    }

    public void ToggleDisplay()
    {
        isDisplaying = !isDisplaying;
        gameObject.SetActive(isDisplaying);
    }

    public void Init(BuildingData[] buildingsData)
    {
        HelperTools.NormalizePool(ref buildingButtons, buildingButtonPrefab, transform, GameManager.Instance.PlayData.isBaseEstablished ? (buildingsData.Length - 1) : 1);
        demolishButton.Init(toggleGroup, OnDemolishSelect, OnDemolishDeselect);
        int index = 0;
        for (int i = 0; i < buildingsData.Length; i++)
        {
            if ((GameManager.Instance.PlayData.isBaseEstablished && buildingsData[i].type == EBuildingType.Base) || (!GameManager.Instance.PlayData.isBaseEstablished && buildingsData[i].type != EBuildingType.Base))
                continue;
            var playData = GameManager.Instance.PlayData;
            bool buildingPossible = playData.IsEnoughResources(buildingsData[i]) && (playData.isBaseEstablished || buildingsData[i].type == EBuildingType.Base);
            buildingButtons[index].Init(buildingsData[i], toggleGroup, OnBuildingSelect, OnBuildingDeselect, buildingPossible);
            index++;
        }
    }

    public void OnDemolishSelect()
    {
        GameManager.Instance.isDemolishing = true;
    }

    public void OnDemolishDeselect()
    {
        GameManager.Instance.isDemolishing = false;
    }

    public void OnBuildingSelect(EBuildingType buildingType)
    {
        GameManager.Instance.OnBuildSelected(buildingType);
    }

    public void OnBuildingDeselect(EBuildingType buildingType)
    {
        GameManager.Instance.OnBuildDeselected(buildingType);
    }
}
