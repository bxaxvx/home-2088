﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UILine : MonoBehaviour
{
    [SerializeField]
    private SpriteRenderer lineRendere;

    public void DrawHorizontal(float verticalOffset, float width)
    {
        transform.position = new Vector3(0, verticalOffset, 10);
        transform.localScale = new Vector3(width, 1, 1);
    }

    public void DrawVertical(float horizontalOffset, float height)
    {
        transform.position = new Vector3(horizontalOffset, 0, 10);
        transform.localScale = new Vector3(1, height, 1);
    }
}
