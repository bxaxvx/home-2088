﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine.Audio;
using TMPro;

public class UIManager : MonoBehaviour
{
    public static UIManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<UIManager>();
            }

            return instance;
        }
    }

    private static UIManager instance;

    [SerializeField]
    private ObjectiveUI objectiveUI;
    [SerializeField]
    private GameObject patterDisplayPrefab;
    [SerializeField]
    private Transform patternsHolder;
    [SerializeField]
    public ToggleGroup patternsToggleGroup;
    [SerializeField]
    public TextMeshProUGUI controlsText;
    [SerializeField]
    public InfoPanelUI infoPanel;
    [SerializeField]
    public ResourcePanelUI resourcePanelUI;
    [SerializeField]
    public Announcer eventAnnouncer;
    [SerializeField]
    public StoryUI storyUI;
    [SerializeField]
    private GameObject blockGO;

    public ModalDialog modalDialog;

    [SerializeField]
    public BuildingsPanelUI buildingsPanel;


    public bool IsBlocked
    {
        get
        {
            return blockGO.activeSelf;
        }
    }

    public void ToggleBlock(bool active)
    {
        blockGO.SetActive(active);
    }

    public void StartUI()
    {
        InitUI();
        storyUI.Display(0, "I was sent to this lifeless planet. Let's see what I could do.");
        var difficulty = GameManager.Instance.PlayData.difficulty;
        if (difficulty == EDifficulty.Sandbox)
            objectiveUI.Hide();
        else
            objectiveUI.Init(GameManager.Instance.scenarioData.PopulationGoal(difficulty), GameManager.Instance.scenarioData.TurnLimit(difficulty));
    }

    private void InitUI()
    {
        buildingsPanel.Init(DataManager.Instance.buildingsData);
    }

    private List<TerraformData> dataToDisplay;

    public void DisplayTerraformOptions(int number, List<ETerraformClass> eRarity)
    {
        var data = new List<TerraformData>(DataManager.Instance.TerraformData(eRarity));
        List<TerraformData> dataToDisplay = new List<TerraformData>();
        for (int i = 0; i < number; i++)
        {
            var pattern = data[Random.Range(0, data.Count)];
            data.Remove(pattern);
            dataToDisplay.Add(pattern);
        }
        this.dataToDisplay = dataToDisplay;
        DisplayTerraformOptions(dataToDisplay);
    }

    public void UpdateTerraformOptions()
    {
        if (dataToDisplay != null)
            DisplayTerraformOptions(dataToDisplay);
    }

    private List<PatternDisplayUI> patternDisplayUI = new List<PatternDisplayUI>();

    private void DisplayTerraformOptions(List<TerraformData> data)
    {
        HelperTools.NormalizePool(ref patternDisplayUI, patterDisplayPrefab, patternsHolder, data.Count);
        for (int i = 0; i < data.Count; i++)
            patternDisplayUI[i].Init(data[i], patternsToggleGroup);
    }

    public void CancelPattern()
    {
        patternsToggleGroup.SetAllTogglesOff();
        foreach (var pattern in patternDisplayUI)
            pattern.gameObject.SetActive(false);
        GameManager.Instance.ResetTilesUI();
    }

    public static bool IsPointerOverUI
    {
        get
        {
            PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
            eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            List<RaycastResult> results = new List<RaycastResult>();
            EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
            return results.Count > 0;
        }
    }

    public static Vector3 MouseToWorldPosition
    {
        get
        {
            Vector3 worldMousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            worldMousePosition.z = 0;
            return worldMousePosition;
        }
    }
}
