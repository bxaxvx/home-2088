﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuScreen : MonoBehaviour
{
    [SerializeField]
    private DifficultyPickerUI diffucultyPicker;

    public void QuitGame()
    {
        Application.Quit();
    }

    public void StartGame()
    {
        GameManager.Instance.StartGame(diffucultyPicker.Difficulty);
        UIManager.Instance.StartUI();
        Hide();
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }
}
