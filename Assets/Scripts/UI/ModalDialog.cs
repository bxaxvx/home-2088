﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ModalDialog : MonoBehaviour
{
    public TextMeshProUGUI titleText;
    public TextMeshProUGUI descriptionText;
    public ExtendedButton button1;
    public ExtendedButton button2;
    public ExtendedButton button3;

    public Action hideCallback;

    public void Hide()
    {
        gameObject.SetActive(false);
        if (hideCallback != null)
            hideCallback();
        hideCallback = null;
    }

    public void Show(string title, string description, string text1 = null)
    {
        titleText.text = title;
        descriptionText.text = description;
        if (text1 == null)
            button1.buttonText.text = "Ok";
        else
            button1.buttonText.text = text1;
        button1.onClick.RemoveAllListeners();
        button1.onClick.AddListener(Hide);
        button2.gameObject.SetActive(false);
        button3.gameObject.SetActive(false);
        gameObject.SetActive(true);
    }

    public void Show(string title, string description, UnityAction customAction)
    {
        titleText.text = title;
        descriptionText.text = description;
        button1.buttonText.text = "Ok";
        button1.onClick.RemoveAllListeners();
        button1.onClick.AddListener(Hide);
        button1.onClick.AddListener(customAction);
        button2.gameObject.SetActive(false);
        button3.gameObject.SetActive(false);
        gameObject.SetActive(true);
    }

    public void Show(string title, string description, string text1, UnityAction customAction1, string text2, UnityAction customAction2 = null)
    {
        titleText.text = title;
        descriptionText.text = description;
        button1.buttonText.text = text1;
        button1.onClick.RemoveAllListeners();
        button1.onClick.AddListener(Hide);
        button1.onClick.AddListener(customAction1);

        button2.buttonText.text = text2;
        button2.onClick.RemoveAllListeners();
        button2.onClick.AddListener(Hide);
        if (customAction2 != null)
            button2.onClick.AddListener(customAction2);

        button2.gameObject.SetActive(true);
        button3.gameObject.SetActive(false);
        gameObject.SetActive(true);
    }

    public void Show(string title, string description, string text1, UnityAction customAction1, string text2, UnityAction customAction2, string text3)
    {
        titleText.text = title;
        descriptionText.text = description;
        button1.buttonText.text = text1;
        button1.onClick.RemoveAllListeners();
        button1.onClick.AddListener(Hide);
        button1.onClick.AddListener(customAction1);

        button2.buttonText.text = text2;
        button2.onClick.RemoveAllListeners();
        button2.onClick.AddListener(Hide);
        button2.onClick.AddListener(customAction2);
        button2.gameObject.SetActive(true);

        button3.buttonText.text = text3;
        button3.gameObject.SetActive(true);
        button3.onClick.RemoveAllListeners();
        button3.onClick.AddListener(Hide);

        gameObject.SetActive(true);
    }
}
