﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class AnnounceUI : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI text1;
    [SerializeField]
    private TextMeshProUGUI text2;
    [SerializeField]
    private TextMeshProUGUI text3;

    public void Init(PeopleArriveEvent peopleEvent, int turnsRemain)
    {
        text1.color = turnsRemain == 1 ? Color.green : Color.white;
        text2.color = turnsRemain == 1 ? Color.green : Color.white;
        text3.color = turnsRemain == 1 ? Color.green : Color.white;

        text1.text = "+" + peopleEvent.numberOfArrivedPeople;
        text2.text = turnsRemain.ToString();
    }

    public void Init(TerraformEvent terraformEvent, int turnsRemain)
    {
        text1.color = turnsRemain == 1 ? Color.green : Color.white;
        text2.color = turnsRemain == 1 ? Color.green : Color.white;
        text1.text = "Terraform in";
        text2.text = turnsRemain.ToString();
    }
}
