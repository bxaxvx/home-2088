﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[AddComponentMenu("UI/Extended Toggle", 30)]
[CanEditMultipleObjects]
[CustomEditor(typeof(ExtendedToggle))]
public class ExtendedToggleEditor : UnityEditor.UI.ToggleEditor
{
    private SerializedProperty onMouseEnterProperty;
    private SerializedProperty onMouseExitProperty;
    private SerializedProperty ignoreScalingObjectsProperty;
    private SerializedProperty mouseDownAudio;
    private SerializedProperty mouseUpAudio;
    private SerializedProperty mouseEnterAudio;
    private SerializedProperty mouseExitAudio;

    protected override void OnEnable()
    {
        base.OnEnable();
        onMouseEnterProperty = serializedObject.FindProperty("onMouseEnter");
        onMouseExitProperty = serializedObject.FindProperty("onMouseExit");
        ignoreScalingObjectsProperty = serializedObject.FindProperty("ignoreScalingObjects");

        mouseDownAudio = serializedObject.FindProperty("mouseDownAudio");
        mouseUpAudio = serializedObject.FindProperty("mouseUpAudio");
        mouseEnterAudio = serializedObject.FindProperty("mouseEnterAudio");
        mouseExitAudio = serializedObject.FindProperty("mouseExitAudio");
    }

    public override void OnInspectorGUI()
    {
        ExtendedToggle component = (ExtendedToggle)target;
        base.OnInspectorGUI();
        component.highlightScaleFactor = EditorGUILayout.FloatField("Hover scale factor", component.highlightScaleFactor);
        component.scaleWhenSelected = EditorGUILayout.Toggle("Keep scaled when selected", component.scaleWhenSelected);
        component.animateScaling = EditorGUILayout.Toggle("Animate hover scaling", component.animateScaling);
        component.animationDuration = EditorGUILayout.FloatField("Hover animation duration", component.animationDuration);
        EditorGUILayout.PropertyField(ignoreScalingObjectsProperty, true);
        //component.tooltip = (TooltipUI)EditorGUILayout.ObjectField("Tooltip", component.tooltip, typeof(TooltipUI), true);
        EditorGUILayout.PropertyField(onMouseEnterProperty, true);
        EditorGUILayout.PropertyField(onMouseExitProperty, true);
        EditorGUILayout.PropertyField(mouseDownAudio, true);
        EditorGUILayout.PropertyField(mouseUpAudio, true);
        EditorGUILayout.PropertyField(mouseEnterAudio, true);
        EditorGUILayout.PropertyField(mouseExitAudio, true);
        serializedObject.ApplyModifiedProperties();
    }
}
