﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ObjectiveUI : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI populationText;
    [SerializeField]
    private TextMeshProUGUI turnText;
    [SerializeField]
    private GameObject turnGO;
    [SerializeField]
    private GameObject objectiveGO;

    public void Init(int targetPopulation, int maxTurn)
    {
        turnGO.SetActive(maxTurn != int.MaxValue);
        populationText.text = targetPopulation.ToString();
        turnText.text = maxTurn.ToString();
        objectiveGO.SetActive(true);
    }

    public void Hide()
    {
        objectiveGO.SetActive(false);
    }
}
