﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuildingButton : MonoBehaviour
{
    [SerializeField]
    private Toggle buildingToggle;
    [SerializeField]
    private Image buildingImage;

    private Action<EBuildingType> onSelectBuilding;
    private Action<EBuildingType> onDeselectBuilding;
    private Action onSelectDemolish;
    private Action onDeselectDemolish;

    private BuildingData data;
    private bool isBuildAvailable;
    private bool isDemolishButton;

    public void Init(BuildingData data, ToggleGroup toggleGroup, Action<EBuildingType> onSelectBuilding, Action<EBuildingType> onDeselectBuilding, bool isBuildingAvailabe)
    {
        isDemolishButton = false;
        isBuildAvailable = isBuildingAvailabe;
        this.onSelectBuilding = onSelectBuilding;
        this.onDeselectBuilding = onDeselectBuilding;
        this.data = data;
        buildingToggle.group = toggleGroup;
        buildingImage.sprite = data.buildingSpritesTier1[0];
        buildingImage.color = new Color(1f, 1f, 1f, isBuildingAvailabe ? 1f : 0.25f);
    }

    public void Init(ToggleGroup toggleGroup, Action onSelectDemolish, Action onDeselectDemolish)
    {
        isDemolishButton = true;
        isBuildAvailable = true;
        this.onSelectDemolish = onSelectDemolish;
        this.onDeselectDemolish = onDeselectDemolish;
        data = null;
        buildingToggle.group = toggleGroup;
        buildingImage.color = new Color(1f, 1f, 1f, 1f);
    }

    public void OnToggleSelect(bool isActive)
    {
        if (isActive)
        {
            if (isBuildAvailable && !isDemolishButton)
                onSelectBuilding(data.type);
            if (isDemolishButton)
                onSelectDemolish();
            if (isDemolishButton)
                UIManager.Instance.infoPanel.InitDemolish(buildingImage.sprite);
            else
                UIManager.Instance.infoPanel.Init(data);
        }
        else
        {
            if (isDemolishButton)
                onDeselectDemolish();
            else
                onDeselectBuilding(data.type);
            UIManager.Instance.infoPanel.Hide();
        }
    }
}
