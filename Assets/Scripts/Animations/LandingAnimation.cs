﻿using EZCameraShake;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LandingAnimation : MonoBehaviour
{
    private const int LANDING_TILES_NUM = 16;
    private const float LANDING_TIME = 4f;
    private const float ANIMATION_SPEED = 8f;

    [SerializeField]
    private SpriteRenderer spriteRenderer;
    [SerializeField]
    private FrameAnimation frameAnimation;
    [SerializeField]
    private Sprite[] landing1Sprites;
    [SerializeField]
    private Sprite[] landing2Sprites;
    [SerializeField]
    private Sprite[] landingStoppingSprites;
    [SerializeField]
    private Sprite[] landingParkingSprites;

    public void PlayAnimation(Location landingLocation, Action onLandedCallback)
    {
        var width = GameManager.Instance.CurrentLevel.Dimension.x;
        var sortingOrder = (-landingLocation.y * width - landingLocation.x) * 3 + 300;
        spriteRenderer.sortingOrder = sortingOrder;
        gameObject.SetActive(true);
        StartCoroutine(PlayAnimation(landingLocation.ToWorldCoordinates() + new Vector3(0, LANDING_TILES_NUM * Tile.TILE_HEIGHT - 6), landingLocation.ToWorldCoordinates() + new Vector3(0, -6), onLandedCallback));
    }

    private IEnumerator PlayAnimation(Vector3 fromLocation, Vector3 toLocation, Action onLandedCallback)
    {
        ShakeCamera();
        transform.position = fromLocation;
        frameAnimation.PlayLoop(spriteRenderer, landing1Sprites, ANIMATION_SPEED);
        LeanTween.move(gameObject, toLocation, LANDING_TIME).setEase(LeanTweenType.easeOutCubic);
        yield return new WaitForSeconds(LANDING_TIME / 3f - 5 * (1f / ANIMATION_SPEED));
        frameAnimation.PlaySingle(spriteRenderer, landingStoppingSprites, ANIMATION_SPEED);
        yield return new WaitForSeconds(5 * (1f / ANIMATION_SPEED));
        frameAnimation.PlayLoop(spriteRenderer, landing2Sprites, ANIMATION_SPEED);
        yield return new WaitForSeconds((2f * LANDING_TIME) / 3f);
        frameAnimation.PlaySingle(spriteRenderer, landingParkingSprites, ANIMATION_SPEED);
        onLandedCallback();
        yield return new WaitForSeconds(7 * (1f / ANIMATION_SPEED));
        gameObject.SetActive(false);
    }

    private void ShakeCamera()
    {
        CameraShakeInstance shakeProperty = new CameraShakeInstance(12, 5, (2f*LANDING_TIME)/3f, LANDING_TIME / 3f);
        shakeProperty.PositionInfluence = new Vector3(1, 0.8f);
        shakeProperty.RotationInfluence = new Vector3(0, 0, 0.1f);
        CameraShaker.Instance.Shake(shakeProperty);
    }
}
