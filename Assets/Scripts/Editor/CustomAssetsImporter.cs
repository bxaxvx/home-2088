﻿using UnityEngine;
using UnityEditor;

public class CustomAssetImporter : AssetPostprocessor
{
    private const int RAILS_OFFSET = 3;
    private const int TILE_SIZE = 16;

    private void OnPreprocessTexture()
    {
        TextureImporter textureImporter = (TextureImporter)assetImporter;
        var filePathDivided = assetPath.Split('/');
        if (filePathDivided.Length > 4 && filePathDivided[1] == "Art" && filePathDivided[2] == "Level")
        {
            textureImporter.spritePackingTag = "level";
            textureImporter.spritePixelsPerUnit = 1;
        }
        textureImporter.filterMode = FilterMode.Point;
        textureImporter.textureCompression = TextureImporterCompression.Compressed;
    }

    private void OnPostprocessSprites(Texture2D texture, Sprite[] sprites)
    {
        ApplyPivotSettings(texture);
    }

    private void ApplyPivotSettings(Texture2D texture)
    {
        TextureImporter textureImporter = (TextureImporter)assetImporter;
        var filePathDivided = assetPath.Split('/');
        if (filePathDivided.Length > 4 && filePathDivided[1] == "Art" && filePathDivided[2] == "Level")
        {
            TextureImporterSettings texSettings = new TextureImporterSettings();
            textureImporter.ReadTextureSettings(texSettings);
            texSettings.spriteAlignment = (int)SpriteAlignment.BottomLeft;
            textureImporter.SetTextureSettings(texSettings);
        }
    }
}
