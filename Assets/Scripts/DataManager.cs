﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataManager : MonoBehaviour
{
    private static DataManager instance;

    public static DataManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<DataManager>();
            }

            return instance;
        }
    }

    public BuildingData[] buildingsData;
    public TerraformData[] terraformData;
    public TerrainData[] terrainData;

    public Sprite[] terraformIcons;
    public Sprite[] terraformCrossIcons;

    public List<TerraformData> TerraformData(List<ETerraformClass> types)
    {
        List<TerraformData> data = new List<TerraformData>();
        foreach (var terData in terraformData)
            if (types.Contains(terData.type))
                data.Add(terData);
        return data;
    }

    public BuildingData GetBuildingData(EBuildingType buildingType)
    {
        return buildingsData[(int)buildingType];
    }

    public TerrainData GetTerrainData(ETerrainType terrainType)
    {
        return terrainData.GetObject(terrainType.ToString());
    }

    public Sprite GetTerrainSprite(ETerrainType terrainType)
    {
        return GetTerrainData(terrainType).terrainSprites[0];
    }
}
